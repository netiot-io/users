FROM anapsix/alpine-java:8
ADD target/users.jar users.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /users.jar
