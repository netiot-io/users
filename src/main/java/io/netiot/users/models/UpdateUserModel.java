package io.netiot.users.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UpdateUserModel {

    @NotNull(message = "backend.users.user.id.notnull")
    private Long id;

    @NotEmpty(message = "backend.users.user.firstname.notnull.not.empty")
    private String firstName;

    @NotEmpty(message = "backend.users.user.lastname.notnull.not.empty")
    private String lastName;

    private String role;

    private String phoneNumber;

    private String country;

    private String address;

    private String postalCode;

    private Long organizationId;

}
