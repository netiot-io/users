package io.netiot.users.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@AllArgsConstructor
@Getter
public abstract class TemplateEmailModel {

    private final String[] receivers;
    private final String subject;
    public abstract Map<String, String> getContextVariables();

    public abstract String getTemplateName();

}