package io.netiot.users.models;

import java.util.HashMap;
import java.util.Map;

public class SetPasswordAccountMailModel extends TemplateEmailModel {

    public static final String TEMPLATE_NAME = "setPassword";
    private final String title;
    private final String message;
    private final String token;
    private final Boolean activate;
    private final String link;

    public SetPasswordAccountMailModel(final String[] destination,
                                       final String subject,
                                       final String title,
                                       final String message,
                                       final String token,
                                       final Boolean activate,
                                       final String link) {
        super(destination, "NetIoT Platform - " + subject);
        this.title = title;
        this.message = message;
        this.token = token;
        this.activate = activate;
        this.link = link;
    }

    @Override
    public Map<String, String> getContextVariables() {
        Map<String, String> arguments = new HashMap<>();
        arguments.put("title", title);
        arguments.put("message", message);
        arguments.put("token", token);
        arguments.put("activate", activate ? "1" : "0");
        arguments.put("link", link);
        return arguments;
    }

    @Override
    public String getTemplateName() {
        return TEMPLATE_NAME;
    }

}
