package io.netiot.users.models;

public enum OrganizationTypeModel {
    ENTERPRISE, PARTNER
}
