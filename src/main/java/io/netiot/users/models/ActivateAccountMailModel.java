package io.netiot.users.models;

import java.util.HashMap;
import java.util.Map;

public class ActivateAccountMailModel extends TemplateEmailModel {

    public static final String TEMPLATE_NAME = "emailActivateAccount";
    private final String title;
    private final String message;
    private final String token;
    private final String link;

    public ActivateAccountMailModel(final String[] destination,
                                    final String title,
                                    final String message,
                                    final String token,
                                    final String link) {
        super(destination, "NetIoT Platform - Activate Account");
        this.title = title;
        this.message = message;
        this.token = token;
        this.link = link;
    }

    @Override
    public Map<String, String> getContextVariables() {
        Map<String, String> arguments = new HashMap<>();
        arguments.put("title", title);
        arguments.put("message", message);
        arguments.put("token", token);
        arguments.put("link", link);
        return arguments;
    }

    @Override
    public String getTemplateName() {
        return TEMPLATE_NAME;
    }

}
