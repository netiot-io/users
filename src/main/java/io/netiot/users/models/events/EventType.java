package io.netiot.users.models.events;

public enum EventType {
    CREATED, DELETED, CHANGED
}
