package io.netiot.users.models.events;

import io.netiot.users.entities.OrganizationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrganizationEvent {

    private Long id;

    private EventType eventType;

    private OrganizationType type;

    private Long createdBy;

}
