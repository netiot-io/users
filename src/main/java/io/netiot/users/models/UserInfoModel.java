package io.netiot.users.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserInfoModel {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String role;

    private String phoneNumber;

    private String country;

    private String address;

    private String postalCode;

    private Long organizationId;

    private String organizationName;

    private String organizationPhone;

    private String organizationAddress;

}
