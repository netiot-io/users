package io.netiot.users.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.netiot.users.entities.OrganizationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrganizationModel {

    private Long id;

    @NotBlank(message = "backend.users.organization.name.notnull.not.empty")
    private String name;

    private String description;

    private String phone;

    private String address;

    private OrganizationType type;

    private Long createdBy;

}
