package io.netiot.users.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class JwtUserModel {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private Boolean active;

    private Boolean enabled;

    private String role;

    private Long organizationId;

    private String organizationName;

}
