package io.netiot.users.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.netiot.users.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserModel {

    private Long id;

    private Long parentUserId;

    @NotEmpty(message = "backend.users.user.firstname.notnull.not.empty")
    private String firstName;

    @NotEmpty(message = "backend.users.user.lastname.notnull.not.empty")
    private String lastName;

    @NotEmpty(message = "backend.users.user.email.notnull.not.empty")
    @Email(message = "backend.users.user.email.invalid")
    private String email;

    private String password;

    private String phoneNumber;

    private String country;

    private String address;

    private String postalCode;

    private Long organizationId;

    private Boolean active;

    private Role role;

}
