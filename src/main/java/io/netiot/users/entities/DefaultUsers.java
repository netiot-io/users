package io.netiot.users.entities;

public enum DefaultUsers {
    FREE_PARTNER(2), PREMIUM_PARTNER(3);

    private long userId;
    DefaultUsers(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }
}

