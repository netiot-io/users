package io.netiot.users.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "ORGANIZATIONS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(exclude = "users")
public class Organization {

    @Id
    @Column(name = "organization_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ORGANIZATIONS_GEN")
    @SequenceGenerator(name = "SEQ_ORGANIZATIONS_GEN", sequenceName = "SEQ_ORGANIZATION", allocationSize = 1)
    private Long id;

    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private OrganizationType type;

    private String phone;

    private String address;

    private LocalDateTime createdAt;

    private Long createdBy;

    private Boolean active;

    private Boolean deleted;

    @OneToMany(mappedBy="organization")
    private List<User> users;
}
