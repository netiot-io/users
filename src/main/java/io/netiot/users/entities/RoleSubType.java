package io.netiot.users.entities;

public enum RoleSubType {
    ADMIN, USER
}
