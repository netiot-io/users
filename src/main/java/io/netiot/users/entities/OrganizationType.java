package io.netiot.users.entities;

public enum OrganizationType {
    ENTERPRISE, PARTNER, SIMPLE
}
