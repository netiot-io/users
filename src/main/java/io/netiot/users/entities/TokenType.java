package io.netiot.users.entities;

public enum TokenType {

    ACTIVATION, RESET_PASSWORD

}
