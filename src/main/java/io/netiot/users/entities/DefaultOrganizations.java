package io.netiot.users.entities;

public enum DefaultOrganizations {
    ENTERPRISE_FREE(2), ENTERPRISE_PREMIUM(3);

    private final long organizationId;
    DefaultOrganizations (long organizationId) {
        this.organizationId = organizationId;
    }

    public long getOrganizationId() {
        return organizationId;
    }
}
