package io.netiot.users.configurations;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface KafkaChannels {

    String ORGANIZATIONS_OUTPUT = "organizations";

    @Output(KafkaChannels.ORGANIZATIONS_OUTPUT)
    MessageChannel organizationOutput();

}
