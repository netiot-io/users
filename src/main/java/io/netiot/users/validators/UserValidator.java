package io.netiot.users.validators;

import io.netiot.users.entities.OrganizationType;
import io.netiot.users.entities.Role;
import io.netiot.users.entities.User;
import io.netiot.users.exceptions.PasswordException;
import io.netiot.users.exceptions.UnauthorizedOperationException;
import io.netiot.users.exceptions.UserException;
import io.netiot.users.models.UserModel;
import io.netiot.users.repositories.UserRepository;
import io.netiot.users.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.users.utils.ErrorMessages.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserValidator {

    private final UserRepository userRepository;
    private final AuthenticatedUserInfo authenticatedUserInfo;

    public void validateOnSave(final UserModel userModel) {
        userRepository.emailUsed(userModel.getEmail())
                .ifPresent( user -> { throw new UserException(EMAIL_ALREADY_USED_ERROR_CODE); });
    }

    public void validateOnUpdate(final User user, final Boolean roleUpdated) {
        Role authenticatedUserRole = authenticatedUserInfo.getRole();
        Long userId = authenticatedUserInfo.getId();
        if(authenticatedUserRole != Role.ADMIN) {
            if(!userId.equals(user.getId())){
                canBeUpdatedBeOtherUser(user, authenticatedUserRole);
            } else {
                if(roleUpdated){
                    throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ROLE_ERROR_CODE);
                }
            }
        }
    }

    public void validateOnUpdateFields(final User user) {
        Role authenticatedUserRole = authenticatedUserInfo.getRole();
        Long userId = authenticatedUserInfo.getId();
        if(userId.equals(user.getId())){
            throw new UserException(USER_CANNOT_CHANGE_ITSELF_ERROR_CODE);
        }
        if(authenticatedUserRole != Role.ADMIN) {
            canBeUpdatedBeOtherUser(user, authenticatedUserRole);
        }
    }

    private void canBeUpdatedBeOtherUser(final User user, final Role authenticatedUserRole) {
        Long authenticatedUserOrganizationId = authenticatedUserInfo.getOrganizationId();
        Long userOrganizationId = user.getOrganization().getId();
        OrganizationType userOrganizationType = user.getOrganization().getType();
        Long userOrganizationCreatedBy = user.getOrganization().getCreatedBy();

        if(userOrganizationType == OrganizationType.SIMPLE || authenticatedUserRole == Role.FREE_USER
                || authenticatedUserRole == Role.PREMIUM_USER) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
        }

        OrganizationType authenticatedUserOrganizationType;
        if(authenticatedUserRole == Role.PARTNER_ADMIN || authenticatedUserRole == Role.PARTNER_USER){
            authenticatedUserOrganizationType = OrganizationType.PARTNER;
        } else {
            authenticatedUserOrganizationType = OrganizationType.ENTERPRISE;
        }

        if((authenticatedUserOrganizationType == OrganizationType.PARTNER)) {
            if(authenticatedUserRole == Role.PARTNER_ADMIN){
                if(userOrganizationType == OrganizationType.PARTNER && !authenticatedUserOrganizationId.equals(userOrganizationId) ){
                    throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);

                }
                if(userOrganizationType != OrganizationType.PARTNER && !authenticatedUserOrganizationId.equals(userOrganizationCreatedBy)) {
                    throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);

                }
            }
            if(authenticatedUserRole == Role.PARTNER_USER){
                if(userOrganizationType == OrganizationType.PARTNER  || !authenticatedUserOrganizationId.equals(userOrganizationCreatedBy)) {
                    throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                }
            }
        }

        if((authenticatedUserOrganizationType == OrganizationType.ENTERPRISE)) {
            if(!userOrganizationId.equals(authenticatedUserOrganizationId)){
                throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

    public void validatePassword(final String password) {
        if(password == null || password.isEmpty()) {
            throw new PasswordException(PASSWORD_MUST_BE_NOT_NULL_OR_EMPTY_ERROR_CODE);
        }
    }

    public void validateOnRead(final User user) {
        Role authenticatedUserRole = authenticatedUserInfo.getRole();
        Long authenticatedUserId = authenticatedUserInfo.getId();
        Long userOrganizationId = user.getOrganization().getId();
        OrganizationType userOrganizationType = user.getOrganization().getType();
        Long userOrganizationCreatedBy = user.getOrganization().getCreatedBy();


        if(authenticatedUserRole != Role.ADMIN) {
            Long authenticatedUserOrganizationId = authenticatedUserInfo.getOrganizationId();

            if (!authenticatedUserId.equals(user.getId())) {

                if (authenticatedUserRole == Role.PARTNER_ADMIN) {
                    if (userOrganizationType == OrganizationType.PARTNER) {
                        if (!authenticatedUserOrganizationId.equals(userOrganizationId)) {
                            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                        }
                    } else if (!authenticatedUserOrganizationId.equals(userOrganizationCreatedBy)) {
                        throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                    }
                }

                if (authenticatedUserRole == Role.PARTNER_USER) {
                    if (userOrganizationType != OrganizationType.ENTERPRISE || !authenticatedUserOrganizationId.equals(userOrganizationCreatedBy)) {
                        throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                    }
                }

                if (authenticatedUserRole == Role.ENTERPRISE_ADMIN) {
                    if (userOrganizationType != OrganizationType.ENTERPRISE || !authenticatedUserOrganizationId.equals(userOrganizationId)) {
                        throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                    }
                }

                if (authenticatedUserRole == Role.PREMIUM_USER || authenticatedUserRole == Role.FREE_USER || authenticatedUserRole == Role.ENTERPRISE_USER) {
                    throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                }

            }
        }
    }
}
