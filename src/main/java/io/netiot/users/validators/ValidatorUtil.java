package io.netiot.users.validators;

import io.netiot.users.exceptions.BindingResultException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ValidatorUtil {

    public void checkBindingResultErrors(final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<String> errorsList = bindingResult.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());

            throw new BindingResultException(errorsList);
        }
    }
}
