package io.netiot.users.validators;

import io.netiot.users.entities.Organization;
import io.netiot.users.entities.OrganizationType;
import io.netiot.users.entities.Role;
import io.netiot.users.exceptions.OrganizationException;
import io.netiot.users.models.OrganizationModel;
import io.netiot.users.repositories.OrganizationRepository;
import io.netiot.users.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.users.utils.ErrorMessages.UNAUTHORIZED_ACTION_ERROR_CODE;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrganizationValidator {

    private final OrganizationRepository organizationRepository;
    private final AuthenticatedUserInfo authenticatedUserInfo;

    public void validate(final OrganizationModel organizationModel, final String errorMessage) {
        organizationRepository.findByName(organizationModel.getName())
                .filter(organization -> !organization.getId().equals(organizationModel.getId()))
                .ifPresent( organization -> { throw new OrganizationException(errorMessage); });
    }

    public void validateOnDelete(final Organization organization) {
        Role authenticatedUserRole = authenticatedUserInfo.getRole();
        Long authenticatedUserOrganizationId = authenticatedUserInfo.getOrganizationId();

        if(authenticatedUserRole != Role.ADMIN){
            if(!organization.getCreatedBy().equals(authenticatedUserOrganizationId)){
                throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }
    }

    public void validateGetOrganizationUsers(final Organization organization){
        Long organizationId = organization.getId();
        OrganizationType organizationType = organization.getType();
        Role role = authenticatedUserInfo.getRole();

        if(role != Role.ADMIN){
            if(organizationType == OrganizationType.SIMPLE){
                throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
            Long userOrganizationId = authenticatedUserInfo.getOrganizationId();
            if(role == Role.PARTNER_ADMIN || role == Role.PARTNER_USER){
                if(organizationType == OrganizationType.PARTNER && !organizationId.equals(userOrganizationId)){
                    throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                }
                if(organizationType == OrganizationType.ENTERPRISE && !organization.getCreatedBy().equals(userOrganizationId)){
                    throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                }
            }
            if(role == Role.ENTERPRISE_ADMIN){
                if(!userOrganizationId.equals(organizationId)){
                    throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
                }
            }
        }
    }

}
