package io.netiot.users.exceptions;

public class SendMailException extends RuntimeException {

    public SendMailException() {
    }

    public SendMailException(String message) {
        super(message);
    }
}
