package io.netiot.users.exceptions;

public class TokenException extends RuntimeException {

    public TokenException() {
    }

    public TokenException(String message) {
        super(message);
    }
}
