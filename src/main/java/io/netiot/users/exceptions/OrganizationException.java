package io.netiot.users.exceptions;

public class OrganizationException extends RuntimeException {

    public OrganizationException() {
    }

    public OrganizationException(String message) {
        super(message);
    }
}
