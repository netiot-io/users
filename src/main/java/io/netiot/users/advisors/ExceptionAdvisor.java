package io.netiot.users.advisors;

import io.netiot.users.exceptions.*;
import io.netiot.users.models.ErrorModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public final class ExceptionAdvisor {

    @ExceptionHandler(value = {BindingResultException.class})
    public ResponseEntity errorHandle(final BindingResultException bindingResultRuntimeException) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorModel
                        .builder()
                        .error(bindingResultRuntimeException.getErrors())
                        .build());
    }

    @ExceptionHandler(value = {PasswordException.class})
    public ResponseEntity errorHandle(final PasswordException passwordException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", passwordException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {UserException.class})
    public ResponseEntity errorHandle(final UserException userException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", userException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {OrganizationException.class})
    public ResponseEntity errorHandle(final OrganizationException organizationException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", organizationException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {TokenException.class})
    public ResponseEntity errorHandle(final TokenException tokenException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", tokenException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

}