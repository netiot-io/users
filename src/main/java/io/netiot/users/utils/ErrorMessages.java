package io.netiot.users.utils;

public final class ErrorMessages {

    public static final String UNAUTHORIZED_ACTION_ERROR_CODE = "backend.users.unauthorized.action";
    public static final String UNAUTHORIZED_ACTION_ROLE_ERROR_CODE = "backend.users.unauthorized.action.role";

    public static final String EMAIL_ALREADY_USED_ERROR_CODE = "backend.users.email.already.used";
    public static final String USER_NOT_EXIST_ERROR_CODE = "backend.users.user.not.exist";
    public static final String USER_CANNOT_CHANGE_ITSELF_ERROR_CODE = "backend.users.user.cannot.change.itself";

    public static final String ORGANIZATION_NAME_ALREADY_EXIST_ERROR_CODE = "backend.users.organization.name.already.used";
    public static final String ORGANIZATION_ALREADY_EXIST_ERROR_CODE = "backend.users.organization.already.exist";
    public static final String ORGANIZATION_ID_MUST_BE_NOT_NULL_ERROR_CODE = "backend.users.organization.must.be.not.null";
    public static final String ORGANIZATION_NOT_EXIST_ERROR_CODE = "backend.users.organization.not.exist";
    public static final String ORGANIZATION_IS_NOT_ASSIGN_TO_A_PARTNER_ERROR_CODE = "backend.users.not.assign.partner.organization";

    public static final String TOKEN_NOT_EXIST_ERROR_CODE = "backend.users.token.not.exist";
    public static final String TOKEN_EXPIRED_ERROR_CODE = "backend.users.token.expired";


    public static final String PASSWORD_MUST_BE_NOT_NULL_OR_EMPTY_ERROR_CODE = "backend.users.password.notnull.not.empty";

}
