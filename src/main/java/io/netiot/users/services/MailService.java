package io.netiot.users.services;

import io.netiot.users.exceptions.SendMailException;
import io.netiot.users.models.EmailModel;
import io.netiot.users.models.TemplateEmailModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender emailSender;
    private final TemplateEngine templateEngine;

    public void send(final EmailModel email) {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setSubject(email.getSubject());
            helper.setTo(email.getReceivers());
            helper.setText(email.getMessage(), true);
            emailSender.send(message);

        } catch (final MessagingException | MailSendException mailException) {
            throw new SendMailException(mailException.toString());
        }
    }

    @Async
    public void send(final TemplateEmailModel templateEmail) {
        Context context = new Context();
        templateEmail.getContextVariables().forEach(context::setVariable);
        String message = templateEngine.process(templateEmail.getTemplateName(), context);
        this.send(EmailModel.builder()
                .receivers(templateEmail.getReceivers())
                .subject(templateEmail.getSubject())
                .message(message).build());
    }

}