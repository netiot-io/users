package io.netiot.users.services;

import io.netiot.users.entities.*;
import io.netiot.users.exceptions.PasswordException;
import io.netiot.users.exceptions.TokenException;
import io.netiot.users.exceptions.UnauthorizedOperationException;
import io.netiot.users.exceptions.UserException;
import io.netiot.users.mappers.UserMapper;
import io.netiot.users.models.*;
import io.netiot.users.repositories.UserRepository;
import io.netiot.users.utils.AuthenticatedUserInfo;
import io.netiot.users.validators.UserValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static io.netiot.users.utils.ErrorMessages.TOKEN_NOT_EXIST_ERROR_CODE;
import static io.netiot.users.utils.ErrorMessages.UNAUTHORIZED_ACTION_ERROR_CODE;
import static io.netiot.users.utils.ErrorMessages.USER_NOT_EXIST_ERROR_CODE;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    public final static List<Role> ADMIN_ROLES = Arrays.asList(Role.PARTNER_ADMIN, Role.ENTERPRISE_ADMIN);

    private final OrganizationService organizationService;
    private final TokenService tokenService;
    private final MailService mailService;
    private final UserRepository userRepository;
    private final UserValidator userValidator;
    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final PasswordEncoder passwordEncoder;
    private final String activationLink;
    private final String setPasswordLink;

    @Autowired
    public UserService(final OrganizationService organizationService, final TokenService tokenService,
                       final MailService mailService, final UserRepository userRepository,
                       final AuthenticatedUserInfo authenticatedUserInfo, final UserValidator userValidator,
                       final PasswordEncoder passwordEncoder,
                       @Value("${account.activation-link}") final String activationLink,
                       @Value("${account.set-password}") final String setPasswordLink) {
        this.organizationService = Objects.requireNonNull(organizationService,"organizationService must not be null.");
        this.tokenService = Objects.requireNonNull(tokenService,"tokenService must not be null.");
        this.mailService = Objects.requireNonNull(mailService,"mailService must not be null.");
        this.userRepository = Objects.requireNonNull(userRepository,"userRepository must not be null.");
        this.userValidator = Objects.requireNonNull(userValidator,"administratorValidator must not be null.");
        this.authenticatedUserInfo = Objects.requireNonNull(authenticatedUserInfo,"userInfoUtils must not be null.");
        this.passwordEncoder = Objects.requireNonNull(passwordEncoder,"passwordEncoder must not be null.");
        this.activationLink = Objects.requireNonNull(activationLink," activationLink not be null.");
        this.setPasswordLink = Objects.requireNonNull(setPasswordLink," setPasswordLink not be null.");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UserInfoModel saveSimpleUser(final UserModel userModel) {
        userValidator.validateOnSave(userModel);
        userValidator.validatePassword(userModel.getPassword());

        User user = generateUserBase(userModel);
        user.setRole(Role.FREE_USER);

        setUserEncryptPassword(user, userModel.getPassword());

        user.setOrganization(organizationService.findOrganization(DefaultOrganizations.ENTERPRISE_FREE.getOrganizationId()));
        user.setParentUser(userRepository.findById(DefaultUsers.FREE_PARTNER.getUserId()).orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); }));

        user = userRepository.save(user);
        Token token = tokenService.generateToken(user, TokenType.ACTIVATION);
        sendActivationAccountMail(token);
        return UserMapper.toUserInfoModel(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UserInfoModel saveAdminUser(final UserModel userModel) {
        userValidator.validateOnSave(userModel);
        User user = generateUserBase(userModel);
        user.setRole(Role.ADMIN);
        user.setPassword(null);
        user = userRepository.save(user);
        Token token = tokenService.generateToken(user, TokenType.ACTIVATION);
        sendActivationAccountMail(token);
        return UserMapper.toUserInfoModel(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UserInfoModel saveEnterpriseUser(final UserModel userModel, final RoleSubType roleSubType, final Long organizationId) {
        userValidator.validateOnSave(userModel);
        User user = generateUserBase(userModel);
        user.setRole(getRoleName(roleSubType, OrganizationType.ENTERPRISE));

        Organization organization = organizationService.findOrganization(organizationId, OrganizationType.ENTERPRISE);
        user.setOrganization(organization);
        user.setParentUser(userRepository.findById(authenticatedUserInfo.getId()).orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); }));
        user.setPassword(null);

        Role authenticatedUserRole = authenticatedUserInfo.getRole();
        if ((authenticatedUserRole == Role.PARTNER_USER || authenticatedUserRole == Role.PARTNER_ADMIN)
                && !organization.getCreatedBy().equals(authenticatedUserInfo.getOrganizationId())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
        }

        if (authenticatedUserRole == Role.ENTERPRISE_ADMIN && !organization.getId().equals(authenticatedUserInfo.getOrganizationId())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
        }
        user = userRepository.save(user);
        Token token = tokenService.generateToken(user, TokenType.ACTIVATION);
        sendActivationAccountMail(token);
        return UserMapper.toUserInfoModel(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UserInfoModel savePartnerUser(final UserModel userModel, final RoleSubType roleSubType, final Long organizationId) {
        userValidator.validateOnSave(userModel);
        User user = UserMapper.toEntity(userModel);
        user.setId(null);
        Organization organization = organizationService.findOrganization(organizationId, OrganizationType.PARTNER);
        user.setOrganization(organization);
        user.setParentUser(userRepository.findById(authenticatedUserInfo.getId())
                .orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); }));
        user.setRole(getRoleName(roleSubType, OrganizationType.PARTNER));
        user.setActive(Boolean.TRUE);
        user.setEnabled(Boolean.FALSE);
        user.setDeleted(Boolean.FALSE);
        user.setPassword(null);

        Role authenticatedUserRole = authenticatedUserInfo.getRole();

        if (authenticatedUserRole == Role.PARTNER_ADMIN) {
            Long authenticatedUserOrganizationId = authenticatedUserInfo.getOrganizationId();
            if(!authenticatedUserOrganizationId.equals(organizationId)){
                throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }

        user = userRepository.save(user);
        Token token = tokenService.generateToken(user, TokenType.ACTIVATION);
        sendActivationAccountMail(token);
        return UserMapper.toUserInfoModel(user);
    }

    private User generateUserBase(final UserModel userModel) {
        User user = UserMapper.toEntity(userModel);
        user.setId(null);
        user.setActive(Boolean.TRUE);
        user.setEnabled(Boolean.FALSE);
        user.setDeleted(Boolean.FALSE);

        return user;
    }

    public Role getRoleName(final RoleSubType roleSubType, final OrganizationType organizationType){
        if(organizationType.equals(OrganizationType.ENTERPRISE)){
            return roleSubType == RoleSubType.ADMIN ? Role.ENTERPRISE_ADMIN : Role.ENTERPRISE_USER;
        }
        if(organizationType.equals(OrganizationType.PARTNER)){
            return roleSubType == RoleSubType.ADMIN ? Role.PARTNER_ADMIN : Role.PARTNER_USER;
        }

        return Role.FREE_USER;
    }

    public UserModel getUser(final Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });

        userValidator.validateOnRead(user);

        return UserMapper.toModel(user);
    }

    public Set<UserModel> getUsersFamily(final List<Long> usersIds) {
        Set<UserModel> users = new HashSet<>();
        for(Long userId: usersIds) {
            User user = userRepository.findById(userId).orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });
            while (user.getRole() != Role.ADMIN) {
                users.add(UserMapper.toModel(user));
                user = user.getParentUser();
            }
        }
        return users;
    }

    public Optional<JwtUserModel> getJwtUser(final String email) {
        return userRepository.findByEmailAndDeletedIsFalse(email).map(UserMapper::toJwtModel);
    }

    public UserModel getParentUser() {
        User user = userRepository.findById(authenticatedUserInfo.getId()).orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });
        return UserMapper.toModel(user.getParentUser());
    }

    public void activateUser(final String token, final String password) {
        Token userToken = tokenService.getToken(token, TokenType.ACTIVATION).orElseThrow(() -> { throw new TokenException(TOKEN_NOT_EXIST_ERROR_CODE); });
        tokenService.checkValidity(userToken);
        User user = userToken.getUser();
        if(!(user.getRole() == Role.FREE_USER || user.getRole() == Role.PREMIUM_USER)){
            userValidator.validatePassword(password);
            setUserEncryptPassword(user,password);
        }
        user.setEnabled(Boolean.TRUE);
        userRepository.save(user);
        tokenService.deleteToken(userToken.getId());
    }

    public void resetPassword(final String token, final String newPassword) {
        Token userToken = tokenService.getToken(token, TokenType.RESET_PASSWORD).orElseThrow(() -> { throw new TokenException(TOKEN_NOT_EXIST_ERROR_CODE); });
        tokenService.checkValidity(userToken);
        userValidator.validatePassword(newPassword);
        User user = userToken.getUser();
        String newPasswordEncrypted = passwordEncoder.encode(newPassword);
        user.setPassword(newPasswordEncrypted);
        userRepository.save(user);
        tokenService.deleteToken(userToken.getId());
    }

    public void tokenRefresh(final String token) {
        Token userToken = tokenService.refresh(token);
        if(userToken.getType() == TokenType.ACTIVATION){
            sendActivationAccountMail(userToken);
        } else {
            sentResetPasswordMail(userToken.getUser().getEmail());
        }
    }

    public void resendActivationMail(final String email) {
        Optional<Token> optionalToken = tokenService.getTokenByUserEmail(email);
        if(optionalToken.isPresent()) {
            Token token = optionalToken.get();
            if (!tokenService.isValid(token)) {
                Token newToken = tokenService.refresh(token.getToken());
                sendActivationAccountMail(newToken);
            } else {
                sendActivationAccountMail(token);
            }
        }
    }

    public UserInfoModel getLoggedUserInfo() {
        return userRepository.findById(authenticatedUserInfo.getId())
                .map(UserMapper::toUserInfoModel)
                .orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });
    }

    public void editActivationStatus(final Long userId, final Boolean activationStatus) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });
        userValidator.validateOnUpdateFields(user);
        user.setActive(activationStatus);
        userRepository.save(user);
    }

    public void update(final UpdateUserModel updateUserModel) {
        Long userId = updateUserModel.getId();
        User user = userRepository.findByIdAndDeletedIsFalse(userId).orElseThrow(() -> { throw new UserException(); });
        user.setFirstName(updateUserModel.getFirstName());
        user.setLastName(updateUserModel.getLastName());
        user.setCountry(updateUserModel.getCountry());
        user.setAddress(updateUserModel.getAddress());
        user.setPhoneNumber(updateUserModel.getPhoneNumber());
        user.setPostalCode(updateUserModel.getPostalCode());
        user.setOrganization(organizationService.findOrganization(updateUserModel.getOrganizationId()));

        Boolean roleUpdated = Boolean.FALSE;
        String subRole = updateUserModel.getRole();
        if(subRole != null){
            Organization userOrganization = user.getOrganization();
            OrganizationType organizationType = userOrganization.getType();
            user.setRole(getRoleName(RoleSubType.valueOf(subRole), organizationType));
            roleUpdated = Boolean.TRUE;
        }

        userValidator.validateOnUpdate(user, roleUpdated);

        userRepository.save(user);
    }

    public void sentResetPasswordMail(final String email) {
        userRepository.findByEmailAndDeletedIsFalse(email).ifPresent(user -> {
            Token token = tokenService.generateToken(user, TokenType.RESET_PASSWORD);
            String[] emailArray = new String[]{user.getEmail()};
            SetPasswordAccountMailModel setPasswordAccountMailModel = new SetPasswordAccountMailModel(emailArray, "ResetPassword","Hello " + user.getFirstName(),
                    "Message", token.getToken(), Boolean.FALSE ,setPasswordLink);
            mailService.send(setPasswordAccountMailModel);
        });
    }

    @Async
    protected void sendActivationAccountMail(final Token token) {
        User user = token.getUser();
        String[] email = new String[]{user.getEmail()};

        if(user.getRole() == Role.FREE_USER || user.getRole() == Role.PREMIUM_USER){
            ActivateAccountMailModel activateAccountMailModel = new ActivateAccountMailModel(email, "Hello " + user.getFirstName(),
                    "Message", token.getToken(), activationLink);
            mailService.send(activateAccountMailModel);
        } else {
            SetPasswordAccountMailModel setPasswordAccountMailModel = new SetPasswordAccountMailModel(email, "Activate Account","Hello " + user.getFirstName(),
                    "Message", token.getToken(), Boolean.TRUE ,setPasswordLink);
            mailService.send(setPasswordAccountMailModel);
        }
    }

    private void checkOldPassword(final User user, final String oldPassword) {
        if(!passwordEncoder.matches(oldPassword, user.getPassword())){
            throw new PasswordException();
        }
    }

    private void setUserEncryptPassword(final  User user, final String password) {
        String encryptedPassword = passwordEncoder.encode(password);
        user.setPassword(encryptedPassword);
    }

    public void changePassword(final ChangePasswordModel changePasswordModel) {
        String email = authenticatedUserInfo.getEmail();
        User user = userRepository.findByEmail(email).orElseThrow(() -> { throw new UserException(); });
        checkOldPassword(user, changePasswordModel.getOldPassword());

        String newPasswordEncrypted = passwordEncoder.encode(changePasswordModel.getNewPassword());
        user.setPassword(newPasswordEncrypted);

        userRepository.save(user);
    }

    public void deleteUser(final Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> { throw new UserException(USER_NOT_EXIST_ERROR_CODE); });
        user.setDeleted(true);

        userValidator.validateOnUpdateFields(user);

        userRepository.save(user);
    }

    public List<UserInfoModel> getOrganizationAdmins() {
        Long authenticatedUserOrganizationId = authenticatedUserInfo.getOrganizationId();
        return userRepository.findByOrganizationIdAndRoleInAndDeletedIsFalse(authenticatedUserOrganizationId, ADMIN_ROLES)
                .stream()
                .map(UserMapper::toUserInfoModel)
                .collect(Collectors.toList());
    }
}
