package io.netiot.users.services;

import io.netiot.users.configurations.KafkaChannels;
import io.netiot.users.models.events.OrganizationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaSenderService {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaSenderService.class);

    private final KafkaChannels kafkaChannels;

    public void sendToKafka(final OrganizationEvent message) {
        MessageChannel channel = kafkaChannels.organizationOutput();
        Message<OrganizationEvent> createOrganizationEventMessage = MessageBuilder
                .withPayload(message)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();
        sendMessage(channel, createOrganizationEventMessage);
    }

    private void sendMessage(final MessageChannel channel, final Message message) {
        try {
            final boolean messageSent = channel.send(message);
            if (!messageSent) {
                LOG.error("The message could not be sent due to a non-fatal reason:", message.getPayload());
            }
        } catch (RuntimeException ex) {
            LOG.error("Non-recoverable error. Unable to send message='{}'", message.getPayload(), ex);
        }
        LOG.info("Event sent");
    }

}
