package io.netiot.users.services;

import io.netiot.users.entities.Token;
import io.netiot.users.entities.TokenType;
import io.netiot.users.entities.User;
import io.netiot.users.exceptions.TokenException;
import io.netiot.users.repositories.TokenRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static io.netiot.users.utils.ErrorMessages.TOKEN_EXPIRED_ERROR_CODE;
import static io.netiot.users.utils.ErrorMessages.TOKEN_NOT_EXIST_ERROR_CODE;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class TokenService {

    private final static Integer TOKEN_AVALABILITY_HOURS = 24;

    private final TokenRepository tokenRepository;

    public Token generateToken(final User user, TokenType type){
        Token token = Token.builder()
                .token(UUID.randomUUID().toString() + user.getId())
                .type(type)
                .expireDate(LocalDateTime.now().plusHours(TOKEN_AVALABILITY_HOURS))
                .user(user).build();
        return tokenRepository.save(token);
    }


    public Optional<Token> getToken(final String token, final TokenType type) {
        return tokenRepository.findByTokenAndType(token, type);
    }

    public void checkValidity(final Token token) {
        if(!LocalDateTime.now().isBefore(token.getExpireDate())){
            throw new TokenException(TOKEN_EXPIRED_ERROR_CODE);
        }
    }

    public Boolean isValid(final Token token) {
        return LocalDateTime.now().isBefore(token.getExpireDate());
    }

    public void deleteToken(final Long id) {
        tokenRepository.deleteById(id);
    }

    public Token refresh(final String token) {
        Token userToken = tokenRepository.findByToken(token).orElseThrow(() -> { throw new TokenException(TOKEN_NOT_EXIST_ERROR_CODE); });
        userToken.setToken(UUID.randomUUID().toString() + userToken.getUser().getId());
        tokenRepository.save(userToken);
        return userToken;
    }

    public Optional<Token> getTokenByUserEmail(final String email) {
        return tokenRepository.findByUserEmail(email);
    }
}
