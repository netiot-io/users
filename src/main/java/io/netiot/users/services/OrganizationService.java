package io.netiot.users.services;

import io.netiot.users.entities.Organization;
import io.netiot.users.entities.OrganizationType;
import io.netiot.users.entities.Role;
import io.netiot.users.exceptions.OrganizationException;
import io.netiot.users.mappers.OrganizationMapper;
import io.netiot.users.mappers.UserMapper;
import io.netiot.users.models.OrganizationModel;
import io.netiot.users.models.UserInfoModel;
import io.netiot.users.models.events.EventType;
import io.netiot.users.models.events.OrganizationEvent;
import io.netiot.users.repositories.OrganizationRepository;
import io.netiot.users.utils.AuthenticatedUserInfo;
import io.netiot.users.utils.TimeUtils;
import io.netiot.users.validators.OrganizationValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netiot.users.utils.ErrorMessages.*;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final OrganizationValidator organizationValidator;
    private final KafkaSenderService kafkaSenderService;
    private final TimeUtils timeUtils;

    public OrganizationModel save(final OrganizationModel organizationModel, final OrganizationType organizationType, final Long partnerOrganizationId) {
        organizationValidator.validate(organizationModel, ORGANIZATION_ALREADY_EXIST_ERROR_CODE);
        Organization organization = OrganizationMapper.toEntity(organizationModel);
        organization.setId(null);
        organization.setType(organizationType);
        organization.setCreatedAt(timeUtils.getCurrentTime());
        organization.setDeleted(Boolean.FALSE);
        if(organizationType != OrganizationType.SIMPLE) {
            Role authenticatedUserRole = authenticatedUserInfo.getRole();
            if((authenticatedUserRole == Role.PARTNER_USER || authenticatedUserRole == Role.PARTNER_ADMIN)
                    && organizationType == OrganizationType.PARTNER) {
                throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
            if(authenticatedUserRole != Role.ADMIN) {
                Long authenticationUserOrganizationId = authenticatedUserInfo.getOrganizationId();
                organization.setCreatedBy(authenticationUserOrganizationId);
            } else {
                if(organizationType == OrganizationType.ENTERPRISE) {
                    if (partnerOrganizationId != null) {
                        Organization partnerOrganization = findOrganization(partnerOrganizationId);
                        organization.setCreatedBy(partnerOrganization.getId());
                    } else {
                        throw new OrganizationException(ORGANIZATION_IS_NOT_ASSIGN_TO_A_PARTNER_ERROR_CODE);
                    }
                }
            }
        }
        organization.setActive(Boolean.TRUE);

        Organization savedOrganization = organizationRepository.save(organization);
        return OrganizationMapper.toModel(savedOrganization);
    }

    public void update(final OrganizationModel organizationModel) {
        if(organizationModel.getId() == null) {
            throw new OrganizationException(ORGANIZATION_ID_MUST_BE_NOT_NULL_ERROR_CODE);
        }

        Organization organization = organizationRepository.findById(organizationModel.getId())
                .orElseThrow(() -> { throw new OrganizationException(ORGANIZATION_NOT_EXIST_ERROR_CODE); });

        organizationValidator.validate(organizationModel, ORGANIZATION_NAME_ALREADY_EXIST_ERROR_CODE);

        if(authenticatedUserInfo.getRole() != Role.ADMIN &&
                !authenticatedUserInfo.getOrganizationId().equals(organization.getCreatedBy()) &&
                !authenticatedUserInfo.getOrganizationId().equals(organization.getId())) {
                throw new OrganizationException(UNAUTHORIZED_ACTION_ERROR_CODE);
        }

        organization.setName(organizationModel.getName());
        organization.setDescription(organizationModel.getDescription());
        organization.setAddress(organizationModel.getAddress());
        organization.setPhone(organizationModel.getPhone());
        organizationRepository.save(organization);
    }

    public Organization findOrganization(final Long organizationId, final OrganizationType organizationType) {
        return organizationRepository.findByIdAndType(organizationId, organizationType)
                .orElseThrow(() -> { throw new OrganizationException("Organization with id " + organizationId +
                        " of type " + organizationType +" not exist."); });
    }

    public List<OrganizationModel> getOrganizations() {
        Long organizationId = authenticatedUserInfo.getOrganizationId();
        return organizationRepository.findByCreatedByAndTypeAndDeletedIsFalse(organizationId, OrganizationType.ENTERPRISE)
                .stream().map(OrganizationMapper::toModel).collect(Collectors.toList());
    }

    public List<OrganizationModel> getOrganizations(final OrganizationType organizationType) {
        return organizationRepository.findByTypeAndDeletedIsFalse(organizationType).stream()
                .map(OrganizationMapper::toModel).collect(Collectors.toList());
    }

    public List<UserInfoModel> getOrganizationUsers(final Long organizationId) {
        Organization organization = this.findOrganization(organizationId);
        organizationValidator.validateGetOrganizationUsers(organization);
        return organization.getUsers().stream().map(UserMapper::toUserInfoModel).collect(Collectors.toList());
    }

    public Organization findOrganization(final Long organizationId) {
        return organizationRepository.findById(organizationId)
                .orElseThrow(() -> { throw new OrganizationException(ORGANIZATION_NOT_EXIST_ERROR_CODE); });
    }

    public void delete(final Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> { throw new OrganizationException("Organization with id " + organizationId + " not exist."); });

        organizationValidator.validateOnDelete(organization);

        organization.setDeleted(Boolean.TRUE);
        organizationRepository.save(organization);

        OrganizationEvent organizationEvent = OrganizationEvent.builder()
                .id(organization.getId())
                .type(organization.getType())
                .eventType(EventType.DELETED)
                .createdBy(organization.getCreatedBy())
                .build();

        kafkaSenderService.sendToKafka(organizationEvent);
    }

    public Optional<OrganizationModel> findOrganizationModelById(final Long organizationId) {
        return organizationRepository.findByIdAndDeletedIsFalse(organizationId)
                .map(OrganizationMapper::toModel);
    }

}
