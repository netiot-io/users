package io.netiot.users.mappers;

import io.netiot.users.entities.Organization;
import io.netiot.users.models.OrganizationModel;

public final class OrganizationMapper {

    private OrganizationMapper() {
    }

    public static Organization toEntity(final OrganizationModel organizationModel) {

        return Organization.builder()
                .id(organizationModel.getId())
                .name(organizationModel.getName())
                .description(organizationModel.getDescription())
                .address(organizationModel.getAddress())
                .phone(organizationModel.getPhone())
                .build();
    }

    public static OrganizationModel toModel(final Organization organization) {
        return OrganizationModel.builder()
                .id(organization.getId())
                .name(organization.getName())
                .description(organization.getDescription())
                .address(organization.getAddress())
                .phone(organization.getPhone())
                .type(organization.getType())
                .createdBy((organization.getCreatedBy()))
                .build();
    }
}
