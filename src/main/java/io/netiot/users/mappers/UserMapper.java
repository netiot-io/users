package io.netiot.users.mappers;

import io.netiot.users.entities.User;
import io.netiot.users.models.JwtUserModel;
import io.netiot.users.models.UserInfoModel;
import io.netiot.users.models.UserModel;

public final class UserMapper {

    private UserMapper() {
    }

    public static User toEntity(final UserModel userModel) {

        return User.builder()
                .id(userModel.getId())
                .firstName(userModel.getFirstName())
                .lastName(userModel.getLastName())
                .email(userModel.getEmail())
                .password(userModel.getPassword())
                .phoneNumber(userModel.getPhoneNumber())
                .country(userModel.getCountry())
                .address(userModel.getAddress())
                .postalCode(userModel.getPostalCode())
                .active(userModel.getActive())
                .build();
    }

    public static UserModel toModel(final User user) {
        return UserModel.builder()
                .id(user.getId())
                .parentUserId(user.getParentUser().getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail().toLowerCase())
                .phoneNumber(user.getPhoneNumber())
                .country(user.getCountry())
                .address(user.getAddress())
                .postalCode(user.getPostalCode())
                .active(user.getActive())
                .organizationId(user.getOrganization().getId())
                .role(user.getRole())
                .build();
    }

    public static JwtUserModel toJwtModel(final User user) {
        return JwtUserModel.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .password(user.getPassword())
                .active(user.getActive())
                .enabled(user.getEnabled())
                .role(user.getRole().name())
                .organizationId(user.getOrganization() != null ? user.getOrganization().getId() : null)
                .organizationName(user.getOrganization() != null ? user.getOrganization().getName() : null)
                .build();
    }

    public static UserInfoModel toUserInfoModel(final User user) {
        return UserInfoModel.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .role(user.getRole().name())
                .phoneNumber(user.getPhoneNumber())
                .country(user.getCountry())
                .address(user.getAddress())
                .postalCode(user.getPostalCode())
                .organizationId(user.getOrganization() != null ? user.getOrganization().getId() : null)
                .organizationName(user.getOrganization() != null ? user.getOrganization().getName() : null)
                .organizationPhone(user.getOrganization() != null ? user.getOrganization().getPhone() : null)
                .organizationAddress(user.getOrganization() != null ? user.getOrganization().getAddress() : null)
                .build();
    }
}
