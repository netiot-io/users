package io.netiot.users.repositories;

import io.netiot.users.entities.Role;
import io.netiot.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByIdAndDeletedIsFalse(final Long id);

    Optional<User> findByEmail(final String email);

    Optional<User> findByEmailAndDeletedIsFalse(final String email);

    @Query(nativeQuery = true, value = "select email from USERS where email = ?")
    Optional<String> emailUsed(final String email);

    List<User> findByOrganizationIdAndRoleInAndDeletedIsFalse(final Long organizationId, final List<Role> roles);


}
