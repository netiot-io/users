package io.netiot.users.repositories;

import io.netiot.users.entities.Organization;
import io.netiot.users.entities.OrganizationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization,Long> {

    Optional<Organization> findByName(final String name);

    Optional<Organization> findByIdAndType(final Long id, final OrganizationType type);

    Optional<Organization> findByIdAndTypeInAndDeletedIsFalse(final Long id, final List<OrganizationType> organizationTypes);

    Optional<Organization> findByIdAndDeletedIsFalse(final Long id);

    List<Organization> findByCreatedByAndTypeAndDeletedIsFalse(final Long createdBy, final OrganizationType organizationType);

    List<Organization> findByTypeAndDeletedIsFalse(final OrganizationType organizationType);

}
