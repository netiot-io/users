package io.netiot.users.repositories;

import io.netiot.users.entities.Token;
import io.netiot.users.entities.TokenType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<Token,Long> {

    Optional<Token> findByTokenAndType(final String token, final TokenType type);

    Optional<Token> findByUserEmail(final String email);

    Optional<Token> findByToken(final String token);
}
