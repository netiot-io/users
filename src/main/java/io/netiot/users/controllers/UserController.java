package io.netiot.users.controllers;

import io.netiot.users.entities.RoleSubType;
import io.netiot.users.models.*;
import io.netiot.users.services.UserService;
import io.netiot.users.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class  UserController {

    private final UserService userService;
    private final ValidatorUtil validatorUtil;

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN','ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/me")
    public ResponseEntity getLoggedUserInfo() {
        return ResponseEntity.ok(userService.getLoggedUserInfo());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN','ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/{userId}")
    public ResponseEntity getUser(@PathVariable(name ="userId") final Long userId) {
        return ResponseEntity.ok(userService.getUser(userId));
    }

    @GetMapping("/internal/auth")
    public List<JwtUserModel> getJwtUser(@RequestParam(name = "email") final String email) {
        final List<JwtUserModel> jwtUserModels = new ArrayList<>();
        userService.getJwtUser(email).ifPresent(jwtUserModels::add);
        return jwtUserModels;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/register/admin")
    public ResponseEntity saveAdminUser(@RequestBody @Valid final UserModel userModel,
                                        final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        UserInfoModel userInfoModel = userService.saveAdminUser(userModel);
        return new ResponseEntity<>(generateMap(userInfoModel), HttpStatus.OK);
    }

    @PostMapping("/register/simple-user")
    public ResponseEntity saveSimpleUser(@RequestBody @Valid final UserModel userModel,
                                         final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        userService.saveSimpleUser(userModel);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN')")
    @PostMapping("/register/enterprise")
    public ResponseEntity saveEnterpriseUser(@RequestParam(name = "role_type") final RoleSubType roleSubType,
                                             @RequestParam(name = "organization_id") final Long organizationId,
                                             @RequestBody @Valid final UserModel userModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        UserInfoModel userInfoModel = userService.saveEnterpriseUser(userModel, roleSubType, organizationId);
        return new ResponseEntity<>(generateMap(userInfoModel), HttpStatus.OK);
    }

    private Map<String, Object> generateMap(final UserInfoModel userInfoModel) {
        Map<String, Object> result = new HashMap<>();
        result.put("user_id", userInfoModel.getId());
        return result;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN')")
    @PostMapping("/register/partner")
    public ResponseEntity savePartnerUser(@RequestParam(name = "role_type") final RoleSubType roleSubType,
                                          @RequestParam(name = "organization_id") final Long organizationId,
                                          @RequestBody @Valid final UserModel userModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        UserInfoModel userInfoModel = userService.savePartnerUser(userModel, roleSubType, organizationId);
        return new ResponseEntity<>(generateMap(userInfoModel), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN'," +
            "'ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER')")
    @PutMapping
    public ResponseEntity updateUser(@RequestBody @Valid final UpdateUserModel updateUserModel) {

        userService.update(updateUserModel);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{token}/activate")
    public ResponseEntity activateUser(@PathVariable(name ="token") final String token,
                                       @RequestParam(name = "password", required = false) final String password) {
        userService.activateUser(token, password);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{token}/token-refresh")
    public ResponseEntity tokenRefresh(@PathVariable(name ="token") final String token) {
        userService.tokenRefresh(token);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{email}/resend-activation-mail")
    public ResponseEntity resendActivationMail(@PathVariable(name ="email") final String email) {
        userService.resendActivationMail(email);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN')")
    @PostMapping("/{user-id}/change-status-account")
    public ResponseEntity changeUserActivationStatusAccount(@PathVariable(name ="user-id") final Long userId,
                                                  @RequestParam(name = "activation-status") final Boolean activationStatus) {
        userService.editActivationStatus(userId, activationStatus);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN','ENTERPRISE_USER'," +
            "'FREE_USER', 'PREMIUM_USER')")
    @PostMapping("/change-password")
    public ResponseEntity changePassword(@RequestBody final ChangePasswordModel changePasswordModel) {
        userService.changePassword(changePasswordModel);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{token}/reset-password")
    public ResponseEntity resetPassword(@PathVariable(name ="token") final String token,
                                        @RequestParam(name = "new-password") final String newPassword) {
        userService.resetPassword(token, newPassword);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/request-reset-password")
    public ResponseEntity requestResetPassword(@RequestParam(name = "email") final String email) {
        userService.sentResetPasswordMail(email);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{user-id}")
    public ResponseEntity deleteUser(@PathVariable(name ="user-id") final Long userId) {
        userService.deleteUser(userId);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER','ENTERPRISE_ADMIN','ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/parent-user")
    public ResponseEntity getParentUser() {
        return ResponseEntity.ok(userService.getParentUser());
    }

    @GetMapping("/internal/device-users")
    public ResponseEntity getDeviceUsersFamily(@RequestParam List<Long> userIds) {
        return ResponseEntity.ok(userService.getUsersFamily(userIds));
    }
}
