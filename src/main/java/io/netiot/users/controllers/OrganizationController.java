package io.netiot.users.controllers;

import io.netiot.users.entities.OrganizationType;
import io.netiot.users.models.OrganizationModel;
import io.netiot.users.models.OrganizationTypeModel;
import io.netiot.users.services.OrganizationService;
import io.netiot.users.services.UserService;
import io.netiot.users.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/organization")
public class OrganizationController {

    private final OrganizationService organizationService;
    private final UserService userService;
    private final ValidatorUtil validatorUtil;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN', 'PARTNER_USER')")
    public ResponseEntity createOrganization(@RequestBody @Valid OrganizationModel organizationModel,
                                             @RequestParam(required = false) final Long partnerOrganizationId,
                                             @RequestParam OrganizationTypeModel organizationType, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        OrganizationModel savedOrganizationModel = organizationService.save(organizationModel, OrganizationType.valueOf(organizationType.name()), partnerOrganizationId);
        Map<String, Object> result = new HashMap<>();
        result.put("organization_id", savedOrganizationModel.getId());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER', 'ENTERPRISE_ADMIN')")
    @PutMapping
    public ResponseEntity updateOrganization(@RequestBody @Valid OrganizationModel organizationModel, final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        organizationService.update(organizationModel);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('PARTNER_ADMIN','PARTNER_USER')")
    @GetMapping
    public ResponseEntity getAllOrganizationsForAPartner() {
        return ResponseEntity.ok(organizationService.getOrganizations());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/{organizationType}")
    public ResponseEntity getAllOrganizationsByTypeForAdmin(@PathVariable(name = "organizationType") final OrganizationType organizationType) {
        return ResponseEntity.ok(organizationService.getOrganizations(organizationType));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER', 'ENTERPRISE_ADMIN')")
    @GetMapping("/{organizationId}/users")
    public ResponseEntity getOrganizationUsers(@PathVariable final Long organizationId) {
        return ResponseEntity.ok(organizationService.getOrganizationUsers(organizationId));
    }

    @PreAuthorize("hasAnyAuthority('PARTNER_ADMIN','PARTNER_USER', 'ENTERPRISE_ADMIN', 'ENTERPRISE_USER')")
    @GetMapping("/{organizationId}/admins")
    public ResponseEntity getOrganizationAdmin() {
        return ResponseEntity.ok(userService.getOrganizationAdmins());
    }


    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER')")
    @DeleteMapping("/{organizationId}")
    public ResponseEntity deleteOrganization(@PathVariable final Long organizationId) {
        organizationService.delete(organizationId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/internal/{organizationId}")
    public ResponseEntity getSimpleOrEnterpriseOrganization(@PathVariable(name = "organizationId") final Long organizationId) {
        return organizationService.findOrganizationModelById(organizationId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
