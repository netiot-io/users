CREATE SEQUENCE SEQ_ORGANIZATION;

CREATE TABLE ORGANIZATIONS (
  organization_id BIGINT PRIMARY KEY,
  name            CHARACTER VARYING(50)  NOT NULL UNIQUE,
  description     CHARACTER VARYING(300),
  type            CHARACTER VARYING(50)  NOT NULL,
  phone           CHARACTER VARYING(20),
  address         CHARACTER VARYING(150),
  created_by      BIGINT,
  created_at      TIMESTAMP NOT NULL,
  active          BOOLEAN NOT NULL DEFAULT TRUE,
  deleted         BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (created_by) REFERENCES ORGANIZATIONS (organization_id)
);

CREATE INDEX ORGANIZATIONS_NAME ON ORGANIZATIONS (name);

CREATE SEQUENCE SEQ_USER;

CREATE TABLE USERS (
  user_id      BIGINT                 PRIMARY KEY,
  first_name   CHARACTER VARYING(50)  NOT NULL,
  last_name    CHARACTER VARYING(50)  NOT NULL,
  email        CHARACTER VARYING(50)  UNIQUE NOT NULL,
  role         CHARACTER VARYING(30)  NOT NULL,
  country      CHARACTER VARYING(30),
  phone_number CHARACTER VARYING(30),
  address      CHARACTER VARYING(150),
  postal_code  CHARACTER VARYING(40),
  password     VARCHAR(200),
  active       BOOLEAN                NOT NULL DEFAULT TRUE,
  enabled      BOOLEAN                NOT NULL DEFAULT FALSE,
  deleted      BOOLEAN                NOT NULL DEFAULT FALSE,
  organization_id BIGINT,
  parent_user_id  BIGINT,
  FOREIGN KEY (organization_id) REFERENCES ORGANIZATIONS (organization_id),
  FOREIGN KEY (parent_user_id) REFERENCES USERS (user_id)
);

CREATE INDEX USERS_EMAIL ON USERS (email);

CREATE SEQUENCE SEQ_TOKEN;

CREATE TABLE TOKENS (
  token_id    BIGINT PRIMARY KEY NOT NULL,
  token       CHARACTER VARYING(200) UNIQUE NOT NULL,
  type        CHARACTER VARYING(20) NOT NULL,
  user_id     BIGINT UNIQUE NOT NULL,
  expire_date TIMESTAMP NOT NULL,
  FOREIGN KEY (user_id) REFERENCES USERS (user_id)
);

CREATE INDEX TOKENS_TOKEN ON TOKENS (token);

INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(1, 'DEFAULT PARTNER', '', 'PARTNER', 'phone', 'address', null, CURRENT_TIMESTAMP);

INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(2, 'ENTERPRISE FREE', '', 'ENTERPRISE', 'phone', 'address', 1, CURRENT_TIMESTAMP);

INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(3, 'ENTERPRISE PREMIUM', '', 'ENTERPRISE', 'phone', 'address', 1, CURRENT_TIMESTAMP);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled)
VALUES(1, 'test', 'test', 'test@hotmail.com', 'ADMIN', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(2, 'Premium', 'Call Center', 'premium_call_center@netiot.ro', 'PARTNER_ADMIN', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 1, 1);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(3, 'Free', 'Call Center', 'free_call_center@netiot.ro', 'PARTNER_USER', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 1, 2);


INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(4, 'PARTNER 1', '', 'PARTNER', 'phone', 'address', 1, CURRENT_TIMESTAMP);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(4, 'Ion', 'Popescu', 'ionpopescu@gmail.com', 'PARTNER_ADMIN', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 4, 1);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(5, 'alex', 'Popescu', 'alexpopescu@gmail.com', 'PARTNER_USER', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 4, 4);


INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(5, 'PARTNER 2', '', 'PARTNER', 'phone', 'address', 1, CURRENT_TIMESTAMP);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(6, 'Ion', 'Nitu', 'ionnitu@gmail.com', 'PARTNER_ADMIN', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 5, 1);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(7, 'alex', 'Nitu', 'alexnitu@gmail.com', 'PARTNER_USER', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 5, 6);


INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(6, 'ENTERPRISE 1', '', 'ENTERPRISE', 'phone', 'address', 4, CURRENT_TIMESTAMP);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(8, 'Ion', 'Alecu', 'ionalecu@gmail.com', 'ENTERPRISE_ADMIN', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 6, 5);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(9, 'alex', 'Alecu', 'alexalecu@gmail.com', 'ENTERPRISE_USER', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 6, 5);


INSERT INTO ORGANIZATIONS(organization_id, name, description, type, phone, address, created_by, created_at)
VALUES(7, 'ENTERPRISE 2', '', 'ENTERPRISE', 'phone', 'address', 5, CURRENT_TIMESTAMP);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(10, 'Ion', 'Dinu', 'iondinu@gmail.com', 'ENTERPRISE_ADMIN', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 7, 7);

INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(11, 'alex', 'Dinu', 'alexdinu@gmail.com', 'ENTERPRISE_USER', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 7, 7);


INSERT INTO USERS(user_id, first_name, last_name, email, role, password, enabled, organization_id, parent_user_id)
VALUES(12, 'alex', 'Radu', 'alexradu@gmail.com', 'FREE_USER', '$2a$10$hpbDNDGYt9.QX0dRaW0Bvupw4D9..XKADETJTtu1234GSFUdEZH.q', true, 2, 3);

ALTER SEQUENCE SEQ_USER RESTART WITH 13;
ALTER SEQUENCE SEQ_ORGANIZATION RESTART WITH 8;