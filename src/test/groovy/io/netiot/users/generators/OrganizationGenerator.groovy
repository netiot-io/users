package io.netiot.users.generators

import io.netiot.users.entities.Organization
import io.netiot.users.entities.OrganizationType
import io.netiot.users.models.OrganizationModel

import java.time.LocalDateTime

class OrganizationGenerator {

    static aOrganizationModel(Map overrides = [:]) {
        Map values = [
                id         : 1,
                name       : "OrganizationName",
                description: "Description",
                phone      : "phone",
                address    : "address",
        ]
        values << overrides
        return OrganizationModel.newInstance(values)
    }

    static aOrganization(Map overrides = [:]) {
        Map values = [
                id         : 1,
                name       : "OrganizationName",
                description: "Description",
                type       : OrganizationType.SIMPLE,
                phone      : "phone",
                address    : "address",
                createdAt  : null,
                createdBy  : null,
                active     : Boolean.TRUE,
                deleted     : Boolean.FALSE
        ]
        values << overrides
        return Organization.newInstance(values)
    }

}