package io.netiot.users.generators

import io.netiot.users.entities.Role
import io.netiot.users.entities.User
import io.netiot.users.models.JwtUserModel
import io.netiot.users.models.UpdateUserModel
import io.netiot.users.models.UserInfoModel
import io.netiot.users.models.UserModel


class UserGenerator {

    static aUserModel(Map overrides = [:]) {
        Map values = [
                id           : 1L,
                firstName    : "Unit",
                lastName     : "Test",
                email        : "test@gmail.com",
                password     : "password",
                phoneNumber  :  "number",
                country      : "Romania",
                address      : "str Unirii",
                postalCode   : "917372",
                active       : Boolean.TRUE,
                role         : Role.FREE_USER,
                parentUserId : 1L,
                organizationId: 1L
        ]
        values << overrides
        return UserModel.newInstance(values)
    }

    static aJwtUserModel(Map overrides = [:]) {
        Map values = [
                id              : 1L,
                firstName       : "Unit",
                lastName        : "Test",
                email           : "test@gmail.com",
                password        : "password",
                active          : Boolean.TRUE,
                enabled         : Boolean.TRUE,
                role            : "ADMIN",
                organizationId  : 1L,
                organizationName: "OrganizationName",
        ]
        values << overrides
        return JwtUserModel.newInstance(values)
    }

    static aUser(Map overrides = [:]) {
        Map values = [
                id          : 1L,
                firstName   : "Unit",
                lastName    : "Test",
                email       : "test@gmail.com",
                phoneNumber:  "number",
                password    : "password",
                country     : "Romania",
                address     : "str Unirii",
                postalCode  : "917372",
                active      : Boolean.TRUE,
                enabled     : Boolean.FALSE,
                deleted     : Boolean.FALSE,
                role        : Role.FREE_USER,
                organization: OrganizationGenerator.aOrganization(),
                parentUser  : null
        ]
        values << overrides
        return User.newInstance(values)
    }

    static aUserInfoModel(Map overrides = [:]) {
        Map values = [
                id              : 1L,
                firstName       : "Unit",
                lastName        : "Test",
                email           : "test@gmail.com",
                phoneNumber     : "number",
                country         : "Romania",
                address         : "str Unirii",
                postalCode      : "917372",
                role            : Role.FREE_USER,
                organizationId  : OrganizationGenerator.aOrganization().getId(),
                organizationName: OrganizationGenerator.aOrganization().getName(),
                organizationAddress: OrganizationGenerator.aOrganization().getAddress(),
                organizationPhone: OrganizationGenerator.aOrganization().getPhone()
        ]
        values << overrides
        return UserInfoModel.newInstance(values)
    }

    static aUpdateUserModel(Map overrides = [:]) {
        Map values = [
                id              : 1L,
                firstName       : "Unit",
                lastName        : "Test",
                role            : "ADMIN",
                phoneNumber     : "number",
                country         : "Romania",
                address         : "str Unirii",
                postalCode      : "917372",
                organizationId  : 1L
        ]
        values << overrides
        return UpdateUserModel.newInstance(values)
    }

}