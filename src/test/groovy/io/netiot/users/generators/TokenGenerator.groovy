package io.netiot.users.generators

import io.netiot.users.entities.Token
import io.netiot.users.entities.TokenType

import java.time.LocalDateTime

class TokenGenerator {

    static aToken(Map overrides = [:]) {
        Map values = [
                id        : 1,
                token     : "token",
                type      : TokenType.ACTIVATION,
                expireDate: LocalDateTime.now(),
                user      : UserGenerator.aUser()
        ]
        values << overrides
        return Token.newInstance(values)
    }

}