package io.netiot.users.generators

import io.netiot.users.entities.Role
import io.netiot.users.entities.User
import io.netiot.users.models.ChangePasswordModel
import io.netiot.users.models.JwtUserModel
import io.netiot.users.models.UserModel


class ChangePasswordModelGenerator {

    static aChangePasswordModel(Map overrides = [:]) {
        Map values = [
                oldPassword: "old",
                newPassword : "new"
        ]
        values << overrides
        return ChangePasswordModel.newInstance(values)
    }

}