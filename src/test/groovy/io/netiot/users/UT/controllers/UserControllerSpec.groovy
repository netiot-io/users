package io.netiot.users.UT.controllers

import io.netiot.users.controllers.UserController
import io.netiot.users.entities.RoleSubType
import io.netiot.users.generators.ChangePasswordModelGenerator
import io.netiot.users.services.UserService
import io.netiot.users.validators.ValidatorUtil
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import spock.lang.Specification

import static io.netiot.users.generators.UserGenerator.*

class UserControllerSpec extends Specification {

    def bindingResult = Mock(BindingResult)
    def userService = Mock(UserService)
    def validatorUtil = Mock(ValidatorUtil)
    def userController = new UserController(userService, validatorUtil)

    def 'getUser'() {
        given:
        def userId = 1L
        def userModel = aUserModel()

        when:
        def entityResponse = userController.getUser(userId)

        then:
        1 * userService.getUser(userId) >> userModel
        0 * _
        entityResponse == ResponseEntity.ok(userModel)
    }

    def 'getJwtUser'() {
        given:
        def email = "test@gmail.com"
        def jwtUserModel = aJwtUserModel()

        when:
        def response = userController.getJwtUser(email)

        then:
        1 * userService.getJwtUser(email) >> Optional.ofNullable(jwtUserModel)
        0 * _
        response == [jwtUserModel]
    }

    def 'saveAdminUser'() {
        given:
        def userModel = aUserModel()
        def userInfoModel = aUserInfoModel()

        when:
        def response = userController.saveAdminUser(userModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * userService.saveAdminUser(userModel) >> userInfoModel
        0 * _
        response == ResponseEntity.ok([user_id: userModel.getId()])
    }

    def 'saveSimpleUser'() {
        given:
        def userModel = aUserModel()

        when:
        def response = userController.saveSimpleUser(userModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * userService.saveSimpleUser(userModel)
        0 * _
        response == ResponseEntity.ok().build()
    }

    def 'saveEnterpriseUser'() {
        given:
        def userModel = aUserModel()
        def roleSubType = RoleSubType.ADMIN
        def organizationId = 1L
        def userInfoModel = aUserInfoModel()

        when:
        def response = userController.saveEnterpriseUser(roleSubType, organizationId, userModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * userService.saveEnterpriseUser(userModel, roleSubType, organizationId) >> userInfoModel
        0 * _
        response == ResponseEntity.ok([user_id: userModel.getId()])
    }

    def 'savePartnerUser'() {
        given:
        def userModel = aUserModel()
        def roleSubType = RoleSubType.ADMIN
        def organizationId = 1L
        def userInfoModel = aUserInfoModel()

        when:
        def response = userController.savePartnerUser(roleSubType, organizationId, userModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * userService.savePartnerUser(userModel, roleSubType, organizationId) >> userInfoModel
        0 * _
        response == ResponseEntity.ok([user_id: userModel.getId()])
    }

    def 'activateUser'() {
        given:
        def token = "token"
        def password = "password"

        when:
        def entityResponse = userController.activateUser(token, password)

        then:
        1 * userService.activateUser(token, password)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'tokenRefresh'() {
        given:
        def token = "token"

        when:
        def entityResponse = userController.tokenRefresh(token)

        then:
        1 * userService.tokenRefresh(token)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'resendActivationMail'() {
        given:
        def token = "resendActivationMail"

        when:
        def entityResponse = userController.resendActivationMail(token)

        then:
        1 * userService.resendActivationMail(token)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'requestResetPassword'() {
        given:
        def email = "email"

        when:
        def entityResponse = userController.requestResetPassword(email)

        then:
        1 * userService.sentResetPasswordMail(email)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'resetPassword'() {
        given:
        def token = "token"
        def newPassword = "newPassword"

        when:
        def entityResponse = userController.resetPassword(token, newPassword)

        then:
        1 * userService.resetPassword(token, newPassword)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'changePassword'() {
        given:
        def changePasswordModel = ChangePasswordModelGenerator.aChangePasswordModel()

        when:
        def entityResponse = userController.changePassword(changePasswordModel)

        then:
        1 * userService.changePassword(changePasswordModel)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'changeUserActivationStatusAccount'() {
        given:
        def userId = 1L
        def activationStatus = Boolean.FALSE

        when:
        def entityResponse = userController.changeUserActivationStatusAccount(userId, activationStatus)

        then:
        1 * userService.editActivationStatus(userId, activationStatus)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'updateUser'() {
        given:
        def updateUserModel = aUpdateUserModel()

        when:
        def entityResponse = userController.updateUser(updateUserModel)

        then:
        1 * userService.update(updateUserModel)
        0 * _
        entityResponse == ResponseEntity.ok().build()
    }

    def 'getLoggedUserInfo'() {
        given:
        def userInfoModel = aUserInfoModel()

        when:
        def entityResponse = userController.getLoggedUserInfo()

        then:
        1 * userService.getLoggedUserInfo() >> userInfoModel
        0 * _
        entityResponse == ResponseEntity.ok(userInfoModel)
    }

}
