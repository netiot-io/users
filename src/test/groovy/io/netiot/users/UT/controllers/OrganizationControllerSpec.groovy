package io.netiot.users.UT.controllers

import io.netiot.users.controllers.OrganizationController
import io.netiot.users.entities.OrganizationType
import io.netiot.users.models.OrganizationTypeModel
import io.netiot.users.services.OrganizationService
import io.netiot.users.services.UserService
import io.netiot.users.validators.ValidatorUtil
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.users.generators.OrganizationGenerator.aOrganizationModel
import static io.netiot.users.generators.UserGenerator.aUserModel

class OrganizationControllerSpec extends Specification {

    def bindingResult = Mock(BindingResult)
    def organizationService = Mock(OrganizationService)
    def userService = Mock(UserService)
    def validatorUtil = Mock(ValidatorUtil)
    def organizationController = new OrganizationController(organizationService, userService, validatorUtil)

    def 'createOrganization'() {
        given:
        def partnerOrganizationId = null
        def organizationTypeModel = OrganizationTypeModel.ENTERPRISE
        def organizationType = OrganizationType.ENTERPRISE
        def organizationModel = aOrganizationModel()
        when:
        def responseEntity = organizationController.createOrganization(organizationModel, partnerOrganizationId, organizationTypeModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        (1 * organizationService.save(organizationModel, organizationType, partnerOrganizationId)) >> organizationModel
        0 * _
        responseEntity == ResponseEntity.ok([organization_id: organizationModel.getId()])
    }

    def 'updateOrganization'() {
        given:
        def organizationModel = aOrganizationModel()
        when:
        def responseEntity = organizationController.updateOrganization(organizationModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * organizationService.update(organizationModel)
        0 * _
        responseEntity == ResponseEntity.ok().build()
    }

    def 'getAllOrganizationsForAPartner'() {
        given:

        when:
        def result = organizationController.getAllOrganizationsForAPartner()

        then:
        1 * organizationService.getOrganizations() >> []
        0 * _
        result == ResponseEntity.ok([])
    }

    def 'getOrganizationUsers'() {
        given:
        def organizationId = 1L

        when:
        def result = organizationController.getOrganizationUsers(organizationId)

        then:
        1 * organizationService.getOrganizationUsers(organizationId) >> []
        0 * _
        result == ResponseEntity.ok([])
    }

    def 'deleteOrganization'() {
        given:
        def organizationId = 1L

        when:
        def result = organizationController.deleteOrganization(organizationId)

        then:
        1 * organizationService.delete(organizationId)
        0 * _
        result == ResponseEntity.ok().build()
    }

    def 'getOrganizationAdmin'() {
        given:
        def admins = [aUserModel()]

        when:
        def result = organizationController.getOrganizationAdmin()

        then:
        1 * userService.getOrganizationAdmins() >> admins
        0 * _
        result == ResponseEntity.ok(admins)
    }

    @Unroll
    def 'getEnterpriseOrganization'() {
        given:
        def organizationId = 1L

        when:
        def result = organizationController.getSimpleOrEnterpriseOrganization(organizationId)

        then:
        1 * organizationService.findOrganizationModelById(organizationId) >> optionalOrganization
        0 * _
        result == expectedResult

        where:
        optionalOrganization                      | expectedResult
        Optional.of(aOrganizationModel())         | ResponseEntity.ok(aOrganizationModel())
        Optional.empty()                          | ResponseEntity.notFound().build()
    }

}
