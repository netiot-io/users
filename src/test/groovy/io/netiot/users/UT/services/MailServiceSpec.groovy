package io.netiot.users.UT.services

import io.netiot.users.exceptions.SendMailException
import io.netiot.users.models.EmailModel
import io.netiot.users.services.MailService
import org.springframework.mail.javamail.JavaMailSender
import org.thymeleaf.TemplateEngine
import spock.lang.Specification

import javax.mail.internet.MimeMessage

class MailServiceSpec extends Specification {

    def javaMailSenderMock = Mock(JavaMailSender)
    def templateEngineMock = Mock(TemplateEngine)
    def session = null
    def mimeMessage = new MimeMessage(session)
    def mailService = new MailService(javaMailSenderMock,templateEngineMock)

    def "send email"() {
        given:
        EmailModel invitationalEmail =
                new EmailModel(["destination"] as String[],"subject","message")


        when:
        mailService.send(invitationalEmail)

        then:
        1 * javaMailSenderMock.createMimeMessage() >> mimeMessage
        1 * javaMailSenderMock.send(mimeMessage)
        0 * _
    }

    def 'send EmailMother with MailSendException'() {
        when:
        mailService.send(new EmailModel([""] as String[],"",""))

        then:
        1 * javaMailSenderMock.createMimeMessage() >> mimeMessage
        0 * _

        and:
        thrown(SendMailException)
    }

}
