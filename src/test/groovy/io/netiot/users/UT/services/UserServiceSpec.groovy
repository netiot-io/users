package io.netiot.users.UT.services

import io.netiot.users.entities.DefaultUsers
import io.netiot.users.entities.OrganizationType
import io.netiot.users.entities.Role
import io.netiot.users.entities.RoleSubType
import io.netiot.users.entities.TokenType
import io.netiot.users.exceptions.TokenException
import io.netiot.users.exceptions.UnauthorizedOperationException
import io.netiot.users.exceptions.UserException
import io.netiot.users.generators.UserGenerator
import io.netiot.users.models.TemplateEmailModel
import io.netiot.users.repositories.UserRepository
import io.netiot.users.services.MailService
import io.netiot.users.services.OrganizationService
import io.netiot.users.services.TokenService
import io.netiot.users.services.UserService
import io.netiot.users.utils.AuthenticatedUserInfo
import io.netiot.users.validators.UserValidator
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification
import spock.lang.Unroll
import io.netiot.users.entities.DefaultOrganizations

import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.TokenGenerator.aToken
import static io.netiot.users.generators.UserGenerator.aUser
import static io.netiot.users.generators.UserGenerator.aUserInfoModel

class UserServiceSpec extends Specification {

    def organizationService = Mock(OrganizationService)
    def tokenService = Mock(TokenService)
    def mailService = Mock(MailService)
    def userRepository = Mock(UserRepository)
    def userValidator = Mock(UserValidator)
    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def passwordEncoder = Mock(PasswordEncoder)
    def activationLink = 'activationLink'
    def setPasswordLink = 'setPasswordLink'
    def userService = new UserService(organizationService, tokenService, mailService, userRepository,
            authenticatedUserInfo, userValidator, passwordEncoder, activationLink, setPasswordLink)

    def 'saveSimpleUser'() {
        given:
        def userModel = UserGenerator.aUserModel()
        def organization = aOrganization(id: 2, name: "ENTERPRISE FREE", description: "", phone: "phone", address: "address",
                createdBy: 1, active: true, deleted: false)
        def defaultUser = aUser(id: 3, organization: organization)
        def user = aUser(id: null, organization: organization, parentUser: defaultUser)

        def rawPassword = "password"
        def encryptedPassword = "password"

        when:
        userService.saveSimpleUser(userModel)

        then:
        1 * userValidator.validateOnSave(userModel)
        1 * userValidator.validatePassword(userModel.getPassword())
        1 * passwordEncoder.encode(rawPassword) >> encryptedPassword
        1 * organizationService.findOrganization(DefaultOrganizations.ENTERPRISE_FREE.getOrganizationId()) >> organization
        1 * userRepository.findById(DefaultUsers.FREE_PARTNER.getUserId()) >> Optional.of(defaultUser)
        1 * userRepository.save(user) >> user
        1 * tokenService.generateToken(user, TokenType.ACTIVATION) >> aToken()
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    def 'saveAdminUser'() {
        given:
        def userModel = UserGenerator.aUserModel()
        def user = aUser(id: null, organization: null, password: null, role: Role.ADMIN)

        when:
        userService.saveAdminUser(userModel)

        then:
        1 * userValidator.validateOnSave(userModel)
        1 * userRepository.save(user) >> user
        1 * tokenService.generateToken(user, TokenType.ACTIVATION) >> aToken()
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    @Unroll
    def 'saveEnterpriseUser'() {
        given:
        def userId = 1L
        def userModel = UserGenerator.aUserModel()
        def organizationId = 1L
        def partnerOrganizationId = 2L
        def organization = aOrganization(id: organizationId, description: "Simple Organization",
                type: OrganizationType.ENTERPRISE, createdBy: partnerOrganizationId)
        def user = aUser(id: null, organization: organization, password: null, role: newUserRole, parentUser: aUser())


        when:
        userService.saveEnterpriseUser(userModel, roleSubType, organizationId)

        then:
        1 * userValidator.validateOnSave(userModel)
        1 * organizationService.findOrganization(organizationId, OrganizationType.ENTERPRISE) >> organization
        1 * authenticatedUserInfo.getId() >> userId
        1 * userRepository.findById(userId) >> Optional.of(aUser())
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> partnerOrganizationId
        1 * userRepository.save(user) >> user
        1 * tokenService.generateToken(user, TokenType.ACTIVATION) >> aToken()
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _

        where:
        roleSubType       | authenticatedUserRole | newUserRole           | getOrganizationIdCalls
        RoleSubType.ADMIN | Role.ADMIN            | Role.ENTERPRISE_ADMIN | 0
        RoleSubType.ADMIN | Role.PARTNER_ADMIN    | Role.ENTERPRISE_ADMIN | 1
        RoleSubType.ADMIN | Role.PARTNER_USER     | Role.ENTERPRISE_ADMIN | 1
        RoleSubType.USER  | Role.ADMIN            | Role.ENTERPRISE_USER  | 0
        RoleSubType.USER  | Role.PARTNER_ADMIN    | Role.ENTERPRISE_USER  | 1
        RoleSubType.USER  | Role.PARTNER_USER     | Role.ENTERPRISE_USER  | 1
    }

    @Unroll
    def 'saveEnterpriseUser thrown UnauthorizedOperationException'() {
        given:
        def userId = 1L
        def userModel = UserGenerator.aUserModel()
        def organizationId = 1L
        def partnerOrganizationId = 2L
        def userOrganizationId = 3L
        def organization = aOrganization(id: organizationId, description: "Simple Organization",
                type: OrganizationType.ENTERPRISE, createdBy: userOrganizationId)

        when:
        userService.saveEnterpriseUser(userModel, roleSubType, organizationId)

        then:
        1 * userValidator.validateOnSave(userModel)
        1 * organizationService.findOrganization(organizationId, OrganizationType.ENTERPRISE) >> organization
        1 * authenticatedUserInfo.getId() >> userId
        1 * userRepository.findById(userId) >> Optional.of(aUser())
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        1 * authenticatedUserInfo.getOrganizationId() >> partnerOrganizationId
        0 * _
        thrown(UnauthorizedOperationException)

        where:
        roleSubType       | authenticatedUserRole
        RoleSubType.ADMIN | Role.PARTNER_ADMIN
        RoleSubType.ADMIN | Role.PARTNER_USER
        RoleSubType.USER  | Role.PARTNER_ADMIN
        RoleSubType.USER  | Role.PARTNER_USER
    }

    @Unroll
    def 'savePartnerUser'() {
        given:
        def userId = 1L
        def userModel = UserGenerator.aUserModel()
        def organizationId = 1L
        def organization = aOrganization(id: organizationId, description: "Simple Organization", type: OrganizationType.ENTERPRISE)
        def user = aUser(id: null, organization: organization, password: null, role: newUserRole, parentUser: aUser())

        when:
        userService.savePartnerUser(userModel, roleSubType, organizationId)

        then:
        1 * userValidator.validateOnSave(userModel)
        1 * organizationService.findOrganization(organizationId, OrganizationType.PARTNER) >> organization
        1 * authenticatedUserInfo.getId() >> userId
        1 * userRepository.findById(userId) >> Optional.of(aUser())
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        authenticatedUserORganizationId * authenticatedUserInfo.getOrganizationId() >> organizationId
        1 * userRepository.save(user) >> user
        1 * tokenService.generateToken(user, TokenType.ACTIVATION) >> aToken()
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _

        where:
        roleSubType       | authenticatedUserRole | newUserRole        | authenticatedUserORganizationId
        RoleSubType.ADMIN | Role.ADMIN            | Role.PARTNER_ADMIN | 0
        RoleSubType.ADMIN | Role.PARTNER_ADMIN    | Role.PARTNER_ADMIN | 1
        RoleSubType.USER  | Role.ADMIN            | Role.PARTNER_USER  | 0
        RoleSubType.USER  | Role.PARTNER_ADMIN    | Role.PARTNER_USER  | 1
    }

    @Unroll
    def 'savePartnerUser thrown UnauthorizedOperationException'() {
        given:
        def userId = 1L
        def userModel = UserGenerator.aUserModel()
        def organizationId = 1L
        def partnerOrganizationId = 2L
        def userOrganizationId = 3L
        def organization = aOrganization(id: organizationId, description: "Simple Organization",
                type: OrganizationType.ENTERPRISE, createdBy: userOrganizationId)

        when:
        userService.savePartnerUser(userModel, roleSubType, organizationId)

        then:
        1 * userValidator.validateOnSave(userModel)
        1 * organizationService.findOrganization(organizationId, OrganizationType.PARTNER) >> organization
        1 * authenticatedUserInfo.getId() >> userId
        1 * userRepository.findById(userId) >> Optional.of(aUser())
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        1 * authenticatedUserInfo.getOrganizationId() >> partnerOrganizationId
        0 * _
        thrown(UnauthorizedOperationException)

        where:
        roleSubType       | authenticatedUserRole
        RoleSubType.ADMIN | Role.PARTNER_ADMIN
        RoleSubType.USER  | Role.PARTNER_ADMIN
    }

    @Unroll
    def 'getRoleName'() {
        when:
        def role = userService.getRoleName(roleSubType, organizationType)

        then:
        role == expectedRole

        where:
        roleSubType       | organizationType            | expectedRole
        RoleSubType.USER  | OrganizationType.SIMPLE     | Role.FREE_USER
        RoleSubType.USER  | OrganizationType.PARTNER    | Role.PARTNER_USER
        RoleSubType.ADMIN | OrganizationType.PARTNER    | Role.PARTNER_ADMIN
        RoleSubType.USER  | OrganizationType.ENTERPRISE | Role.ENTERPRISE_USER
        RoleSubType.ADMIN | OrganizationType.ENTERPRISE | Role.ENTERPRISE_ADMIN
    }

    def 'getUser'() {
        given:
        def userId = 1L

        when:
        def userModel = userService.getUser(userId)

        then:
        1 * userRepository.findById(userId) >> Optional.of(aUser(parentUser: aUser()))
        1 * userValidator.validateOnRead(aUser(parentUser: aUser()))
        0 * _
        userModel == UserGenerator.aUserModel(password: null)
    }

    def 'getUser thrown UserException'() {
        given:
        def userId = 1L

        when:
        userService.getUser(userId)

        then:
        1 * userRepository.findById(userId) >> Optional.empty()
        0 * _
        thrown(UserException)
    }

    def 'getJwtUser'() {
        given:
        def email = "email"

        when:
        def userModelOptional = userService.getJwtUser(email)

        then:
        1 * userRepository.findByEmailAndDeletedIsFalse(email) >> Optional.of(aUser(role: Role.ADMIN, enabled: Boolean.TRUE))
        0 * _
        userModelOptional == Optional.of(UserGenerator.aJwtUserModel())
    }

    def 'getJwtUser get empty optional'() {
        given:
        def email = "email"

        when:
        def userModelOptional = userService.getJwtUser(email)

        then:
        1 * userRepository.findByEmailAndDeletedIsFalse(email) >> Optional.empty()
        0 * _
        userModelOptional == Optional.empty()
    }

    def 'activateUser with role SIMPLE_USER OR PREMIUM_USER'() {
        given:
        def stringToken = "token"
        def password = "password"
        def user = aUser(role: role)
        def token = aToken(user: user)

        when:
        userService.activateUser(stringToken, password)

        then:
        1 * tokenService.getToken(stringToken, TokenType.ACTIVATION) >> Optional.of(token)
        1 * tokenService.checkValidity(token)
        1 * userRepository.save(token.getUser())
        1 * tokenService.deleteToken(token.getId())
        0 * _

        where:
        role              | _
        Role.FREE_USER    | _
        Role.PREMIUM_USER | _
    }

    def 'activateUser with other role that SIMPLE_USER OR PREMIUM_USER'() {
        given:
        def stringToken = "token"
        def password = "password"
        def user = aUser(role: role)
        def token = aToken(user: user)

        when:
        userService.activateUser(stringToken, password)

        then:
        1 * tokenService.getToken(stringToken, TokenType.ACTIVATION) >> Optional.of(token)
        1 * tokenService.checkValidity(token)
        1 * userValidator.validatePassword(password)
        1 * passwordEncoder.encode(password) >> password
        1 * userRepository.save(token.getUser())
        1 * tokenService.deleteToken(token.getId())
        0 * _

        where:
        role                  | _
        Role.PARTNER_ADMIN    | _
        Role.PARTNER_USER     | _
        Role.ENTERPRISE_ADMIN | _
        Role.ENTERPRISE_USER  | _
        Role.ADMIN            | _
    }

    def 'activateUser thrown TokenException'() {
        given:
        def stringToken = "token"
        def password = "password"

        when:
        userService.activateUser(stringToken, password)

        then:
        1 * tokenService.getToken(stringToken, TokenType.ACTIVATION) >> Optional.empty()
        0 * _
        thrown(TokenException)
    }

    def 'tokenRefresh with TokenType.ACTIVATION'() {
        given:
        def stringToken = "token"
        def token = aToken(type : TokenType.ACTIVATION)

        when:
        userService.tokenRefresh(stringToken)

        then:
        1 * tokenService.refresh(stringToken) >> token
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    def 'tokenRefresh with TokenType.RESET_PASSWORD'() {
        given:
        def stringToken = "token"
        def token = aToken(type : TokenType.RESET_PASSWORD)
        def user = token.getUser()

        when:
        userService.tokenRefresh(stringToken)

        then:
        1 * tokenService.refresh(stringToken) >> token
        1 * userRepository.findByEmailAndDeletedIsFalse(user.getEmail()) >> Optional.of(user)
        1 * tokenService.generateToken(user, TokenType.RESET_PASSWORD) >> aToken()
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    def 'tokenRefresh with TokenType.RESET_PASSWORD and email not exist'() {
        given:
        def stringToken = "token"
        def token = aToken(type : TokenType.RESET_PASSWORD)
        def user = token.getUser()

        when:
        userService.tokenRefresh(stringToken)

        then:
        1 * tokenService.refresh(stringToken) >> token
        1 * userRepository.findByEmailAndDeletedIsFalse(user.getEmail()) >> Optional.empty()
        0 * _
    }

    def 'resendActivationMail'() {
        given:
        def email = "email"
        def token = aToken()

        when:
        userService.resendActivationMail(email)

        then:
        1 * tokenService.getTokenByUserEmail(email) >> Optional.of(token)
        1 * tokenService.isValid(token) >> true
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    def 'resendActivationMail with expired token'() {
        given:
        def email = "email"
        def token = aToken()

        when:
        userService.resendActivationMail(email)

        then:
        1 * tokenService.getTokenByUserEmail(email) >> Optional.of(token)
        1 * tokenService.isValid(token) >> false
        1 * tokenService.refresh(token.getToken()) >> token
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    def 'editActivationStatus'() {
        given:
        def userId = 2L
        def activationStatus = false
        def user = aUser()

        when:
        userService.editActivationStatus(userId, activationStatus)

        then:
        1 * userRepository.findById(userId) >> Optional.of(user)
        1 * userValidator.validateOnUpdateFields(user)
        1 * userRepository.save(user)
        0 * _
    }

    def 'editActivationStatus thrown UserException'() {
        given:
        def userId = 2L
        def activationStatus = false

        when:
        userService.editActivationStatus(userId, activationStatus)

        then:
        1 * userRepository.findById(userId) >> Optional.empty()
        0 * _
        thrown(UserException)
    }

    def 'update with role update'() {
        given:
        def userId = 1L
        def organizationId = 1L
        def user = aUser()
        def role = "ADMIN"
        def updateUserModel = UserGenerator.aUpdateUserModel(role: role)
        def organization = aOrganization(id: 1, name: "ENTERPRISE FREE", description: "", phone: "phone", address: "address",
                createdBy: 1, active: true, deleted: false)

        when:
        userService.update(updateUserModel)

        then:
        1 * userRepository.findByIdAndDeletedIsFalse(userId) >> Optional.of(user)
        1 * organizationService.findOrganization(organizationId) >> organization
        1 * userValidator.validateOnUpdate(user, Boolean.TRUE)
        1 * userRepository.save(user)
        0 * _
    }


    def 'update without role update'() {
        given:
        def userId = 1L
        def organizationId = 1L
        def user = aUser()
        def role = null
        def updateUserModel = UserGenerator.aUpdateUserModel(role: role)
        def organization = aOrganization(id: 1, name: "ENTERPRISE FREE", description: "", phone: "phone", address: "address",
                createdBy: 1, active: true, deleted: false)

        when:
        userService.update(updateUserModel)

        then:
        1 * userRepository.findByIdAndDeletedIsFalse(userId) >> Optional.of(user)
        1 * organizationService.findOrganization(organizationId) >> organization
        1 * userValidator.validateOnUpdate(user, Boolean.FALSE)
        1 * userRepository.save(user)
        0 * _
    }

    def 'update thrown UserException'() {
        given:
        def userId = 1L
        def role = "ADMIN"
        def updateUserModel = UserGenerator.aUpdateUserModel(role: role)

        when:
        userService.update(updateUserModel)

        then:
        1 * userRepository.findByIdAndDeletedIsFalse(userId) >> Optional.empty()
        0 * _
        thrown(UserException)
    }

    def 'sentResetPasswordMail'() {
        given:
        def email = "email"
        def user = aUser()
        def token = aToken()

        when:
        userService.sentResetPasswordMail(email)

        then:
        1 * userRepository.findByEmailAndDeletedIsFalse(email) >> Optional.of(user)
        1 * tokenService.generateToken(user, TokenType.RESET_PASSWORD) >> token
        1 * mailService.send(_ as TemplateEmailModel)
        0 * _
    }

    def 'sentResetPasswordMail user not exist'() {
        given:
        def email = "email"

        when:
        userService.sentResetPasswordMail(email)

        then:
        1 * userRepository.findByEmailAndDeletedIsFalse(email) >> Optional.empty()
        0 * _
    }

    def 'resetPassword'() {
        given:
        def token = "token"
        def userToken = aToken(type: TokenType.RESET_PASSWORD)
        def newPassword = "newPassword"
        def passwordEncoded = "passwordEncoded"
        def user = aUser(password: passwordEncoded)

        when:
        userService.resetPassword(token, newPassword)

        then:
        1 * tokenService.getToken(token, TokenType.RESET_PASSWORD) >> Optional.of(userToken)
        1 * tokenService.checkValidity(userToken)
        1 * userValidator.validatePassword(newPassword)
        1 * passwordEncoder.encode(newPassword) >> passwordEncoded
        1 * userRepository.save(user)
        1 * tokenService.deleteToken(userToken.getId())
        0 * _
    }


    def 'resetPassword thrown TokenException'() {
        given:
        def token = "token"
        def newPassword = "newPassword"

        when:
        userService.resetPassword(token, newPassword)

        then:
        1 * tokenService.getToken(token, TokenType.RESET_PASSWORD) >> Optional.empty()
        0 * _
        thrown(TokenException)

    }

    def 'getLoggedUserInfo'() {
        given:
        def userId = 1L

        when:
        def userModel = userService.getLoggedUserInfo()

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * userRepository.findById(userId) >> Optional.of(aUser())
        0 * _
        userModel == aUserInfoModel()
    }

    def 'getLoggedUserInfo thrown UserException'() {
        given:
        def userId = 1L

        when:
        userService.getLoggedUserInfo()

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * userRepository.findById(userId) >> Optional.empty()
        0 * _
        thrown(UserException)
    }

    def 'getOrganizationAdmins'() {
        given:
        def organizationId = 1L

        when:
        def result = userService.getOrganizationAdmins()

        then:
        1 * authenticatedUserInfo.getOrganizationId() >> organizationId
        1 * userRepository.findByOrganizationIdAndRoleInAndDeletedIsFalse(organizationId, UserService.ADMIN_ROLES) >> [aUser()]
        0 * _

        result == [aUserInfoModel()]
    }

}

