package io.netiot.users.UT.services

import io.netiot.users.configurations.KafkaChannels
import io.netiot.users.models.events.OrganizationEvent
import io.netiot.users.services.KafkaSenderService
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import spock.lang.Specification

class KafkaSenderServiceSpec extends Specification {

    def kafkaChannels = Mock(KafkaChannels)
    def sender = new KafkaSenderService(kafkaChannels)
    def messageChannel = Mock(MessageChannel)

    def 'sendToKafka - event'() {
        given:
        def message = new OrganizationEvent()

        when:
        sender.sendToKafka(message)

        then:
        1 * kafkaChannels.organizationOutput() >> messageChannel
        1 * messageChannel.send(_ as Message) >> true
        0 * _
    }

    def 'sendToKafka - event - LOG non-fatal reason error'() {
        given:
        def message = new OrganizationEvent()

        when:
        sender.sendToKafka(message)

        then:
        1 * kafkaChannels.organizationOutput() >> messageChannel
        1 * messageChannel.send(_ as Message) >> false
        0 * _
    }

}
