package io.netiot.users.UT.services

import io.netiot.users.entities.Organization
import io.netiot.users.entities.OrganizationType
import io.netiot.users.entities.Role
import io.netiot.users.exceptions.OrganizationException
import io.netiot.users.generators.OrganizationGenerator
import io.netiot.users.models.events.OrganizationEvent
import io.netiot.users.repositories.OrganizationRepository
import io.netiot.users.services.KafkaSenderService
import io.netiot.users.services.OrganizationService
import io.netiot.users.utils.AuthenticatedUserInfo
import io.netiot.users.utils.TimeUtils
import io.netiot.users.validators.OrganizationValidator
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime

import static io.netiot.users.utils.ErrorMessages.ORGANIZATION_ALREADY_EXIST_ERROR_CODE
import static io.netiot.users.utils.ErrorMessages.ORGANIZATION_NAME_ALREADY_EXIST_ERROR_CODE

class OrganizationServiceSpec extends Specification {

    def organizationRepository = Mock(OrganizationRepository)
    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def organizationValidator = Mock(OrganizationValidator)
    def kafkaSenderService = Mock(KafkaSenderService)
    def timeUtils = Mock(TimeUtils)
    def organizationService = new OrganizationService(organizationRepository, authenticatedUserInfo,
            organizationValidator, kafkaSenderService, timeUtils)

    def 'save Simple Organization'() {
        given:
            def localDateTime = LocalDateTime.now()
            def partnerOrganizationId = null
            def organizationModel = OrganizationGenerator.aOrganizationModel()
            def organizationType = OrganizationType.SIMPLE
            def organization = OrganizationGenerator.aOrganization(createdAt: localDateTime)

        when:
            organizationService.save(organizationModel, organizationType, partnerOrganizationId)
        then:
            1 * organizationValidator.validate(organizationModel, ORGANIZATION_ALREADY_EXIST_ERROR_CODE)
            1 * timeUtils.getCurrentTime() >> localDateTime
            1 * organizationRepository.save(_ as Organization) >> organization
            0 * _
    }

    def 'save Partner Organization'() {
        given:
            def localDateTime = LocalDateTime.now()
            def partnerOrganizationId = null
            def organizationType = OrganizationType.PARTNER
            def organizationModel = OrganizationGenerator.aOrganizationModel()
            def organization = OrganizationGenerator.aOrganization(type: organizationType, createdAt: localDateTime)
        when:
            organizationService.save(organizationModel, organizationType, partnerOrganizationId)
        then:
            1 * organizationValidator.validate(organizationModel, ORGANIZATION_ALREADY_EXIST_ERROR_CODE)
            1 * timeUtils.getCurrentTime() >> localDateTime
            1 * authenticatedUserInfo.getRole() >> Role.ADMIN
            1 * organizationRepository.save(_ as Organization) >> organization
            0 * _
    }

    def 'save Partner Organization be PARTNER_ADMIN user thrown OrganizationException'() {
        given:
            def localDateTime = LocalDateTime.now()
            def partnerOrganizationId = null
            def organizationType = OrganizationType.PARTNER
            def organizationModel = OrganizationGenerator.aOrganizationModel()
        when:
            organizationService.save(organizationModel, organizationType, partnerOrganizationId)
        then:
            1 * organizationValidator.validate(organizationModel, ORGANIZATION_ALREADY_EXIST_ERROR_CODE)
            1 * timeUtils.getCurrentTime() >> localDateTime
            1 * authenticatedUserInfo.getRole() >> Role.PARTNER_ADMIN
            0 * _
            thrown(OrganizationException)
    }

    def 'save Partner Organization be PARTNER_USER user thrown OrganizationException'() {
        given:
            def localDateTime = LocalDateTime.now()
            def partnerOrganizationId = null
            def organizationType = OrganizationType.PARTNER
            def organizationModel = OrganizationGenerator.aOrganizationModel()
        when:
            organizationService.save(organizationModel, organizationType, partnerOrganizationId)
        then:
            1 * organizationValidator.validate(organizationModel, ORGANIZATION_ALREADY_EXIST_ERROR_CODE)
            1 * timeUtils.getCurrentTime() >> localDateTime
            1 * authenticatedUserInfo.getRole() >> Role.PARTNER_USER
            0 * _
            thrown(OrganizationException)
    }

    def 'save Enterprise Organization'() {
        given:
            def localDateTime = LocalDateTime.now()
            def partnerOrganizationId = null
            def organizationType = OrganizationType.ENTERPRISE
            def organizationModel = OrganizationGenerator.aOrganizationModel()
            def organization = OrganizationGenerator.aOrganization(type: organizationType, createdAt: localDateTime, createdBy: 5L)
        when:
            organizationService.save(organizationModel, organizationType, partnerOrganizationId)
        then:
            1 * organizationValidator.validate(organizationModel,ORGANIZATION_ALREADY_EXIST_ERROR_CODE)
            1 * timeUtils.getCurrentTime() >> localDateTime
            1 * authenticatedUserInfo.getRole() >> Role.PARTNER_ADMIN
            1 * authenticatedUserInfo.getOrganizationId() >> 5L
            1 * organizationRepository.save(_ as Organization) >> organization
            0 * _
    }

    def 'update Organization with id null'() {
        given:
        def organizationModel = OrganizationGenerator.aOrganizationModel(id: null)

        when:
        organizationService.update(organizationModel)
        then:
        thrown(OrganizationException)
    }

    def 'update not existed Organization thrown OrganizationException'() {
        given:
            def organizationModel = OrganizationGenerator.aOrganizationModel()
        when:
            organizationService.update(organizationModel)
        then:
            1 * organizationRepository.findById(organizationModel.getId()) >> Optional.empty()
            0 * _
            thrown(OrganizationException)
    }

    def 'update Organization with already used name thrown OrganizationException'() {
        given:
            def organizationModel = OrganizationGenerator.aOrganizationModel()
            def organizationType = OrganizationType.PARTNER
            def organization = OrganizationGenerator.aOrganization(type: organizationType)
        when:
            organizationService.update(organizationModel)
        then:
        1 * organizationRepository.findById(organizationModel.getId()) >> Optional.of(organization)
        1 * organizationValidator.validate(organizationModel, ORGANIZATION_NAME_ALREADY_EXIST_ERROR_CODE) >> { throw new OrganizationException()}
        0 * _
        thrown(OrganizationException)
    }

    @Unroll
    def 'update Organization by PARTNER user from other organization thrown OrganizationException'() {
        given:
        def organizationModel = OrganizationGenerator.aOrganizationModel()
        def organizationType = OrganizationType.PARTNER
        def organization = OrganizationGenerator.aOrganization(type: organizationType, createdBy: organizationCreatedBy)
        when:
        organizationService.update(organizationModel)
        then:
        1 * organizationRepository.findById(organizationModel.getId()) >> Optional.of(organization)
        1 * organizationValidator.validate(organizationModel, ORGANIZATION_NAME_ALREADY_EXIST_ERROR_CODE)
        1 * authenticatedUserInfo.getRole() >> role
        2 * authenticatedUserInfo.getOrganizationId() >> partnerOrganizationId
        0 * _
        thrown(OrganizationException)

        where:
        role                | organizationCreatedBy  | partnerOrganizationId
        Role.PARTNER_USER   |  5L                    |  4L
        Role.PARTNER_ADMIN  |  5L                    |  4L
    }

    @Unroll
    def 'update Organization by PARTNER user from same organization'() {
        given:
        def organizationModel = OrganizationGenerator.aOrganizationModel()
        def organizationType = OrganizationType.PARTNER
        def organization = OrganizationGenerator.aOrganization(type: organizationType, createdBy: organizationCreatedBy)
        when:
        organizationService.update(organizationModel)
        then:
        1 * organizationRepository.findById(organizationModel.getId()) >> Optional.of(organization)
        1 * organizationValidator.validate(organizationModel, ORGANIZATION_NAME_ALREADY_EXIST_ERROR_CODE)
        1 * authenticatedUserInfo.getRole() >> role
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        1 * organizationRepository.save(organization)
        0 * _
        where:
        role               | organizationCreatedBy | userOrganizationId | getOrganizationIdCalls
        Role.PARTNER_USER  | 5L                    | 5L                 | 1
        Role.PARTNER_ADMIN | 5L                    | 5L                 | 1
        Role.ADMIN         | 5L                    | null               | 0
    }

    def 'findOrganization'(){
        given:
        def organizationId = 1L
        def organizationType = OrganizationType.PARTNER
        def organization = OrganizationGenerator.aOrganization()

        when:
        organizationService.findOrganization(organizationId, organizationType)

        then:
        1 *  organizationRepository.findByIdAndType(organizationId, organizationType) >> Optional.of(organization)
        0 * _
    }

    def 'findOrganization thrown OrganizationException'() {
        given:
        def organizationId = 1L
        def organizationType = OrganizationType.PARTNER

        when:
        organizationService.findOrganization(organizationId, organizationType)

        then:
        1 *  organizationRepository.findByIdAndType(organizationId, organizationType) >> Optional.empty()
        0 * _
        thrown(OrganizationException)
    }

    def 'getOrganizations'() {
        given:
        def organizationId = 1L
        def organizationType =  OrganizationType.ENTERPRISE

        when:
        def result = organizationService.getOrganizations()

        then:
        1 * authenticatedUserInfo.getOrganizationId() >> organizationId
        1 * organizationRepository.findByCreatedByAndTypeAndDeletedIsFalse(organizationId, organizationType) >> []
        0 * _

        result == []
    }

    def 'getOrganizations with organizationType'() {
        given:
        def organizationType =  OrganizationType.ENTERPRISE

        when:
        def result = organizationService.getOrganizations(organizationType)

        then:
        1 * organizationRepository.findByTypeAndDeletedIsFalse(organizationType) >> []
        0 * _

        result == []
    }

}
