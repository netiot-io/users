package io.netiot.users.UT.services

import io.netiot.users.entities.Token
import io.netiot.users.entities.TokenType
import io.netiot.users.exceptions.TokenException
import io.netiot.users.generators.TokenGenerator
import io.netiot.users.generators.UserGenerator
import io.netiot.users.repositories.TokenRepository
import io.netiot.users.services.TokenService
import spock.lang.Specification

import java.time.LocalDateTime

class TokenServiceSpec extends Specification {

    def tokenRepository = Mock(TokenRepository)
    def tokenService = new TokenService(tokenRepository)

    def 'generateToken'(){
        given:
            def user = UserGenerator.aUser()

        when:
            tokenService.generateToken(user, TokenType.ACTIVATION)

        then:
            1 * tokenRepository.save(_ as Token) >> TokenGenerator.aToken()
            0 * _
    }

    def 'getToken'(){
        given:
            def stringToken = "Token"
            def tokenType = TokenType.ACTIVATION
            def token = TokenGenerator.aToken()

        when:
            def optional = tokenService.getToken(stringToken, tokenType)

        then:
            1 * tokenRepository.findByTokenAndType(stringToken, tokenType) >> Optional.of(token)
            0 * _
            optional == Optional.of(token)
    }

    def 'checkValidity'(){
        given:
            def token = TokenGenerator.aToken(expireDate: LocalDateTime.now().plusHours(2))

        when:
            tokenService.checkValidity(token)

        then:
            true
    }

    def 'checkValidity thrown TokenException'(){
        given:
            def token = TokenGenerator.aToken(expireDate: LocalDateTime.now().minusDays(2))

        when:
            tokenService.checkValidity(token)

        then:
            thrown(TokenException)
    }

    def 'deleteToken'(){
        given:
            def tokenId = 1L

        when:
            tokenService.deleteToken(tokenId)

        then:
            1 * tokenRepository.deleteById(tokenId)
            0 * _
    }

    def 'refresh'(){
        given:
            def token = TokenGenerator.aToken()

        when:
            def resultToken = tokenService.refresh(token.getToken())

        then:
            1 * tokenRepository.findByToken(token.getToken()) >> Optional.of(token)
            1 * tokenRepository.save(_ as Token) >> token
            0 * _
            resultToken == token
    }

    def 'refresh thrown TokenException'(){
        given:
            def stringToken = "Token"

        when:
            tokenService.refresh(stringToken)

        then:
            1 * tokenRepository.findByToken(stringToken) >> Optional.empty()
            0 * _
            thrown(TokenException)
    }

}
