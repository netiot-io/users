package io.netiot.users.UT.validators

import io.netiot.users.entities.OrganizationType
import io.netiot.users.entities.Role
import io.netiot.users.exceptions.OrganizationException
import io.netiot.users.exceptions.UserException
import io.netiot.users.generators.OrganizationGenerator
import io.netiot.users.repositories.OrganizationRepository
import io.netiot.users.utils.AuthenticatedUserInfo
import io.netiot.users.validators.OrganizationValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.OrganizationGenerator.aOrganization

class OrganizationValidatorSpec extends Specification {

    def organizationRepository = Mock(OrganizationRepository)
    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def organizationValidator = new OrganizationValidator(organizationRepository, authenticatedUserInfo)

    def 'validateOnSave'(){
        given:
        def organizationModel = OrganizationGenerator.aOrganizationModel()
        def errorMessage = "errorMessage"

        when:
        organizationValidator.validate(organizationModel, errorMessage)

        then:
        1 * organizationRepository.findByName(organizationModel.getName()) >> Optional.empty()
        0 * _
    }

    def 'validateOnSave thrown OrganizationException'(){
        given:
        def otherOrganizationId = 4L
        def organizationModel = OrganizationGenerator.aOrganizationModel()
        def organization = OrganizationGenerator.aOrganization(id: otherOrganizationId)
        def errorMessage = "errorMessage"

        when:
        organizationValidator.validate(organizationModel, errorMessage)

        then:
        1 * organizationRepository.findByName(organizationModel.getName()) >> Optional.of(organization)
        0 * _
        thrown(OrganizationException)
    }

    @Unroll
    def 'validateGetOrganizationUsers'() {
        when:
        organizationValidator.validateGetOrganizationUsers(organization)

        then:
        1 * authenticatedUserInfo.getRole() >> userRole
        userGetOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _

        where:
        userRole              | userOrganizationId | organization                                                            | userGetOrganizationIdCalls
        Role.ADMIN            | null               | aOrganization()                                                         | 0
        Role.ADMIN            | null               | aOrganization(id: 1L, type: OrganizationType.SIMPLE)                    | 0
        Role.ADMIN            | null               | aOrganization(id: 1L, type: OrganizationType.ENTERPRISE)                | 0
        Role.ADMIN            | null               | aOrganization(id: 1L, type: OrganizationType.PARTNER)                   | 0
        Role.PARTNER_ADMIN    | 1L                 | aOrganization(id: 1L, type: OrganizationType.PARTNER)                   | 1
        Role.PARTNER_ADMIN    | 1L                 | aOrganization(id: 2L, type: OrganizationType.ENTERPRISE, createdBy: 1L) | 1
        Role.PARTNER_USER     | 1L                 | aOrganization(id: 1L, type: OrganizationType.PARTNER)                   | 1
        Role.PARTNER_USER     | 1L                 | aOrganization(id: 2L, type: OrganizationType.ENTERPRISE, createdBy: 1L) | 1
        Role.ENTERPRISE_ADMIN | 2L                 | aOrganization(id: 2L, type: OrganizationType.ENTERPRISE, createdBy: 1L) | 1
    }

    @Unroll
    def 'validateGetOrganizationUsers thrown OrganizationException'() {
        when:
        organizationValidator.validateGetOrganizationUsers(organization)

        then:
        1 * authenticatedUserInfo.getRole() >> userRole
        userGetOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> userOrganizationId
        0 * _
        thrown(OrganizationException)

        where:
        userRole              | userOrganizationId | organization                                                            | userGetOrganizationIdCalls
        Role.PARTNER_ADMIN    | 1L                 | aOrganization(id: 2L, type: OrganizationType.PARTNER)                   | 1
        Role.PARTNER_ADMIN    | 1L                 | aOrganization(id: 2L, type: OrganizationType.ENTERPRISE, createdBy: 3L) | 1
        Role.PARTNER_USER     | 1L                 | aOrganization(id: 2L, type: OrganizationType.PARTNER)                   | 1
        Role.PARTNER_USER     | 1L                 | aOrganization(id: 2L, type: OrganizationType.ENTERPRISE, createdBy: 3L) | 1
        Role.ENTERPRISE_ADMIN | 3L                 | aOrganization(id: 2L, type: OrganizationType.ENTERPRISE, createdBy: 1L) | 1
    }

}
