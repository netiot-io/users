package io.netiot.users.UT.validators

import io.netiot.users.entities.OrganizationType
import io.netiot.users.entities.Role
import io.netiot.users.exceptions.UnauthorizedOperationException
import io.netiot.users.exceptions.UserException
import io.netiot.users.repositories.UserRepository
import io.netiot.users.utils.AuthenticatedUserInfo
import io.netiot.users.validators.UserValidator
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.users.generators.OrganizationGenerator.aOrganization
import static io.netiot.users.generators.UserGenerator.*

class UserValidatorSpec extends Specification {

    def userRepository = Mock(UserRepository)
    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def userValidator = new UserValidator(userRepository,authenticatedUserInfo)

    @Unroll
    def 'validateOnUpdate'() {
        given:
        def userId = 2L
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)

        when:
        userValidator.validateOnUpdate(user, roleUpdated)

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> authenticatedUserOrganizationId
        0 * _

        where:
        authenticatedUserRole | authenticatedUserOrganizationId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | roleUpdated   | getOrganizationIdCalls
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 0
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 1L                        | Boolean.FALSE | 0
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | Boolean.FALSE | 0
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 1L                        | Boolean.FALSE | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 1L                        | Boolean.FALSE | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.FALSE | 1
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 0
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 1L                        | Boolean.TRUE  | 0
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | Boolean.TRUE  | 0
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 1L                        | Boolean.TRUE  | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 1L                        | Boolean.TRUE  | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.TRUE  | 1
    }

    @Unroll
    def 'validateOnUpdate thrown UnauthorizedOperationException'() {
        given:
        def userId = 2L
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)

        when:
        userValidator.validateOnUpdate(user, roleUpdated)

        then:
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        1 * authenticatedUserInfo.getId() >> userId
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> authenticatedUserOrganizationId
        thrown UnauthorizedOperationException
        0 * _

        where:
        authenticatedUserRole | authenticatedUserOrganizationId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | roleUpdated   | getOrganizationIdCalls
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | Boolean.FALSE | 1
        Role.PARTNER_ADMIN    | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.PARTNER_ADMIN    | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 3L                        | Boolean.FALSE | 1
        Role.PARTNER_USER     | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 3L                        | Boolean.FALSE | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.FALSE | 1
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | Boolean.TRUE  | 1
        Role.PARTNER_ADMIN    | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.PARTNER_ADMIN    | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 3L                        | Boolean.TRUE  | 1
        Role.PARTNER_USER     | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 3L                        | Boolean.TRUE  | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.TRUE  | 1

        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE  | 1
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE | 1
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.TRUE  | 1
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.TRUE  | 1
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.FALSE | 1
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.FALSE | 1
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.TRUE  | 1
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.TRUE  | 1
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.FALSE | 1
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.FALSE | 1

    }

    @Unroll
    def 'validateOnUpdate user update itself'() {
        given:
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)

        when:
        userValidator.validateOnUpdate(user, roleUpdated)

        then:
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        1 * authenticatedUserInfo.getId() >> user.getId()
        0 * _

        where:
        authenticatedUserRole | authenticatedUserOrganizationId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | roleUpdated
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | Boolean.FALSE
        Role.PARTNER_USER     | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.FALSE
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.FALSE
        Role.ENTERPRISE_USER  | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.FALSE
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.FALSE
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.FALSE

    }

    @Unroll
    def 'validateOnUpdate user update itself thrown UnauthorizedOperationException'() {
        given:
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)

        when:
        userValidator.validateOnUpdate(user, roleUpdated)

        then:
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        1 * authenticatedUserInfo.getId() >> user.getId()
        thrown UnauthorizedOperationException
        0 * _

        where:
        authenticatedUserRole | authenticatedUserOrganizationId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | roleUpdated
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | Boolean.TRUE
        Role.PARTNER_USER     | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | Boolean.TRUE
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.TRUE
        Role.ENTERPRISE_USER  | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | Boolean.TRUE
        Role.FREE_USER        | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.TRUE
        Role.PREMIUM_USER     | 1L                              | 2L                 | OrganizationType.SIMPLE     | null                      | Boolean.TRUE

    }

    @Unroll
    def 'validateOnUpdateFields'() {
        given:
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)
        def userId = 2L

        when:
        userValidator.validateOnUpdateFields(user)

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> authenticatedUserOrganizationId
        0 * _

        where:
        authenticatedUserRole | authenticatedUserOrganizationId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | getOrganizationIdCalls
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | 0
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 1L                        | 0
        Role.ADMIN            | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | 0
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | 1
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 1L                        | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 1L                        | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 1L                 | OrganizationType.ENTERPRISE | 5L                        | 1

    }

    @Unroll
    def 'validateOnUpdateFields thrown UnauthorizedOperationException'() {
        given:
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)
        def userId = 2L

        when:
        userValidator.validateOnUpdateFields(user)

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> authenticatedUserOrganizationId
        thrown UnauthorizedOperationException
        0 * _

        where:
        authenticatedUserRole | authenticatedUserOrganizationId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | getOrganizationIdCalls
        Role.PARTNER_ADMIN    | 1L                              | 1L                 | OrganizationType.SIMPLE     | 1L                        | 1
        Role.PARTNER_ADMIN    | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | 1
        Role.PARTNER_ADMIN    | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 3L                        | 1
        Role.PARTNER_USER     | 1L                              | 1L                 | OrganizationType.PARTNER    | null                      | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | 1
        Role.PARTNER_USER     | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 3L                        | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.PARTNER    | null                      | 1
        Role.ENTERPRISE_ADMIN | 1L                              | 2L                 | OrganizationType.ENTERPRISE | 5L                        | 1
    }

    def 'validateOnSave'() {
        given:
        def userModel = aUserModel()

        when:
        userValidator.validateOnSave(userModel)

        then:
        1 * userRepository.emailUsed(userModel.getEmail()) >> Optional.empty()
        0 * _
    }

    def 'validateOnSave thrown UserException'() {
        given:
        def userModel = aUserModel()
        def user = aUser()

        when:
        userValidator.validateOnSave(userModel)

        then:
        1 * userRepository.emailUsed(userModel.getEmail()) >> Optional.of(user.getEmail())
        0 * _
        thrown(UserException)
    }

    @Unroll
    def 'validateOnRead'() {
        given:
        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
        def user = aUser(organization: organization, active: Boolean.FALSE)

        when:
        userValidator.validateOnRead(user)

        then:
        1 * authenticatedUserInfo.getId() >> authenticatedUserId
        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> authenticatedUserOrganizationId
        0 * _

        where:
        authenticatedUserRole | authenticatedUserId | authenticatedUserOrganizationId | userId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | getOrganizationIdCalls
        Role.ADMIN            | 1L                  | 1L                              | 1L     | 1L                 | null                        | null                      | 0
        Role.PARTNER_ADMIN    | 1L                  | 1L                              | 1L     | 1L                 | OrganizationType.PARTNER    | 1L                        | 1
        Role.PARTNER_USER     | 1L                  | 1L                              | 1L     | 1L                 | OrganizationType.PARTNER    | 1L                        | 1
        Role.ENTERPRISE_ADMIN | 1L                  | 1L                              | 1L     | 1L                 | OrganizationType.ENTERPRISE | 1L                        | 1
        Role.ENTERPRISE_USER  | 1L                  | 1L                              | 1L     | 1L                 | OrganizationType.ENTERPRISE | 1L                        | 1
        Role.FREE_USER        | 1L                  | 1L                              | 1L     | 1L                 | OrganizationType.SIMPLE     | 1L                        | 1
        Role.PREMIUM_USER     | 1L                  | 1L                              | 1L     | 1L                 | OrganizationType.SIMPLE     | 1L                        | 1
        Role.PARTNER_ADMIN    | 2L                  | 1L                              | 1L     | 1L                 | OrganizationType.PARTNER    | 1L                        | 1
        Role.PARTNER_ADMIN    | 2L                  | 2L                              | 1L     | 1L                 | OrganizationType.ENTERPRISE | 2L                        | 1
        Role.PARTNER_USER     | 2L                  | 2L                              | 1L     | 1L                 | OrganizationType.ENTERPRISE | 2L                        | 1
        Role.ENTERPRISE_ADMIN | 2L                  | 2L                              | 1L     | 2L                 | OrganizationType.ENTERPRISE | 2L                        | 1
    }
//
//    @Unroll
//    def 'validateOnRead thrown UnauthorizedOperationException'() {
//        given:
//        def organization = aOrganization(id: userOrganizationId, description: "Simple Organization",
//                type: userOrganizationType, createdBy: userOrganizationCreatedBy)
//        def user = aUser(organization: organization, active: Boolean.FALSE)
//
//        when:
//        userValidator.validateOnRead(user)
//
//        then:
//        1 * authenticatedUserInfo.getId() >> authenticatedUserId
//        1 * authenticatedUserInfo.getRole() >> authenticatedUserRole
//        getOrganizationIdCalls * authenticatedUserInfo.getOrganizationId() >> authenticatedUserOrganizationId
//        0 * _
//        thrown(UnauthorizedOperationException)
//
//        where:
//        authenticatedUserRole | authenticatedUserId | authenticatedUserOrganizationId | userId | userOrganizationId | userOrganizationType        | userOrganizationCreatedBy | getOrganizationIdCalls
//        Role.PARTNER_ADMIN    | 2L                  | 2L                              | 1L     | 1L                 | OrganizationType.PARTNER    | 1L                        | 1
//        Role.PARTNER_ADMIN    | 2L                  | 2L                              | 1L     | 1L                 | OrganizationType.ENTERPRISE | 1L                        | 1
//        Role.PARTNER_ADMIN    | 2L                  | 2L                              | 1L     | 1L                 | OrganizationType.SIMPLE     | null                      | 1
//        Role.PARTNER_USER     | 2L                  | 2L                              | 1L     | 1L                 | OrganizationType.PARTNER    | 1L                        | 1
//        Role.PARTNER_USER     | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.PARTNER    | 1L                        | 1
//        Role.PARTNER_USER     | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.SIMPLE     | null                      | 1
//        Role.ENTERPRISE_ADMIN | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.PARTNER    | null                      | 1
//        Role.ENTERPRISE_ADMIN | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.ENTERPRISE | 1L                        | 1
//        Role.ENTERPRISE_ADMIN | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.SIMPLE     | null                      | 1
//        Role.ENTERPRISE_USER  | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.PARTNER    | 1L                        | 1
//        Role.ENTERPRISE_USER  | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.ENTERPRISE | 1L                        | 1
//        Role.ENTERPRISE_USER  | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.SIMPLE     | 1L                        | 1
//        Role.FREE_USER        | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.SIMPLE     | null                      | 1
//        Role.FREE_USER        | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.PARTNER    | 1L                        | 1
//        Role.FREE_USER        | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.ENTERPRISE | 1L                        | 1
//        Role.PREMIUM_USER     | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.SIMPLE     | null                      | 1
//        Role.PREMIUM_USER     | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.PARTNER    | 1L                        | 1
//        Role.PREMIUM_USER     | 2L                  | 3L                              | 1L     | 2L                 | OrganizationType.ENTERPRISE | 1L                        | 1
//
//
//    }

}
