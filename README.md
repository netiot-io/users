# Users

Microservice that handles the users administration.

## Known paths
### user
`GET users-and-organizations/V1/user/me`
```json
{
  "id": 1,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "organization_name": "",
  "organization_address": "",
  "organization_phone": ""
}
```

`GET users-and-organizations/V1/user/{userID}`
```json
{
  "id": 1,
  "parent_user_id": 2,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "password": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "active": true
}
```

`GET users-and-organizations/V1/user/parent-user`

Body
```json
{
  "id": 1,
  "parent_user_id": 2,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "password": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "active": true
}
```

`GET users-and-organizations/V1/user/internal/auth`
```json
{
  "id": 1,
  "first_name": "",
  "last_name": "",
  "email": "",
  "password": "",
  "active": true,
  "enabled": true,
  "role": "",
  "organization_id": 1,
  "organization_name": ""
}
```

`GET users-and-organizations/V1/user/internal/device-users?userIds=1,2,3`
```json
[
    {
      "id": 1,
      "parent_user_id": 2,
      "first_name": "",
      "last_ame": "",
      "email": "",
      "password": "",
      "phone_number": "",
      "country": "",
      "address": "",
      "role": "",
      "postcal_code": "",
      "organization_id": 1,
      "active": true
    }
]
```

`POST users-and-organizations/V1/user/register/admin`

Body
```json
{
  "id": 1,
  "parent_user_id": 2,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "password": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "active": true
}
```

`POST users-and-organizations/V1/user/register/simple-user`

Body
```json
{
  "id": 1,
  "parent_user_id": 2,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "password": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "active": true
}
```

`POST users-and-organizations/V1/user/register/enterprise?role_type=&organization_id=`

Body
```json
{
  "id": 1,
  "parent_user_id": 2,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "password": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "active": true
}
```

`POST users-and-organizations/V1/user/register/partner?role_type=&organization_id=`

Body
```json
{
  "id": 1,
  "parent_user_id": 2,
  "first_name": "",
  "last_ame": "",
  "email": "",
  "password": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "role": "",
  "postcal_code": "",
  "organization_id": 1,
  "active": true
}
```

`POST users-and-organizations/V1/user/{token}/activate?password=`

`POST users-and-organizations/V1/user/{token}/token-refresh`

`POST users-and-organizations/V1/user/{email}/resend-activation-mail`

`POST users-and-organizations/V1/user/{user-id}/change-status-account?activation-status=`

`POST users-and-organizations/V1/user/change-password`

Body
```json
{
  "old_password": "",
  "new_password": ""  
}    
```

`POST users-and-organizations/V1/user/{token}/reset-password?new-password=`

`POST users-and-organizations/V1/user/request-reset-password?email=`

`PUT users-and-organizations/V1/user`

Body
```json
{
  "id": 1,
  "first_name": "",
  "last_ame": "",
  "role": "",
  "phone_number": "",
  "country": "",
  "address": "",
  "postcal_code": "",
  "organization_id": 1
}
```

`DELETE users-and-organizations/V1/user/{user-id}`

### organization

`GET users-and-organizations/V1/organization`

Return
```json
    "id": 1,
    "name": "",
    "description": "",
    "phone": "",
    "address": "",
    "type": "partner",
    "created_by": 2
```

`GET users-and-organizations/V1/organization/{organizationType}`

Return
```json
    "id": 1,
    "name": "",
    "description": "",
    "phone": "",
    "address": "",
    "type": "partner",
    "created_by": 2
```

`GET users-and-organizations/V1/organization/{organizationType}/users`

Return
```json
[
   {
     "id": 1,
     "first_name": "",
     "last_ame": "",
     "email": "",
     "phone_number": "",
     "country": "",
     "address": "",
     "role": "",
     "postcal_code": "",
     "organization_id": 1,
     "organization_name": "",
     "organization_address": "",
     "organization_phone": ""
   }
]
```

`GET users-and-organizations/V1/organization/{organizationId}/admins`

Return
```json
[
   {
     "id": 1,
     "first_name": "",
     "last_ame": "",
     "email": "",
     "phone_number": "",
     "country": "",
     "address": "",
     "role": "",
     "postcal_code": "",
     "organization_id": 1,
     "organization_name": "",
     "organization_address": "",
     "organization_phone": ""
   }
]
```

`GET users-and-organizations/V1/organization/internal/{organizationId}`

Return
```json
    "id": 1,
    "name": "",
    "description": "",
    "phone": "",
    "address": "",
    "type": "partner",
    "created_by": 2
```

`POST users-and-organizations/V1/organization?partnerOrganizationId=&organizationType=`

Body

```json
    "name": "",
    "description": "",
    "phone": "",
    "address": "",
    "type": "partner",
```

`PUT users-and-organizations/V1/organization`

Body

```json
    "name": "",
    "description": "",
    "phone": "",
    "address": "",
    "type": "partner",
```

`DELETE users-and-organizations/V1/organization/{organizationId}`

## DB
Database migration implemented with [flyway](https://flywaydb.org/)
Scrips in `resources/db/migration`.
There are a few users (with different types) added by default to the DB. You can see them in `V1_inital.sql`.
All the passwords are `admin`.

## Kafka
Sends a kafka message on the `organizations` topic defined in config whenever an organization is deleted.

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
Groovy tests in `src/test/groovy`.

## Notes
This microservice sends emails when a user registers and when a password reset is requested.
You need to configure the `mail` section in the config file in order for this to work.
The email templates are located in `resources/templates`.